from argparse import ArgumentParser
import os
import sqlite3
import re
import math
import mdtraj as md
import pandas as pd
import numpy as np
import glob
import os
from TimeSeries import TimeSeries
from matplotlib import pyplot as plt

codeDict1_3 = {"G": "GLY", "A": "ALA", "V": "VAL", "L": "LEU", "I": "ILE", "P": "PRO", "C": "CYS",
               "M": "MET", "W": "TRP", "Y": "TYR", "F": "PHE", "H": "HIS", "R": "ARG", "K": "LYS",
               "D": "ASP", "E": "GLU", "N": "ASN", "Q": "GLN", "S": "SET", "T": "THR"}

codeDict3_1 = {"GLY": "G", "ALA": "A", "VAL": "V", "LEU": "L", "ILE": "I", "PRO": "P", "CYS": "C",
               "MET": "M", "TRP": "W", "TYR": "Y", "PHE": "F", "HIS": "H", "ARG": "R", "LYS": "K",
               "ASP": "D", "GLU": "E", "ASN": "N", "GLN": "Q", "SET": "S", "THR": "T"}

class MDSystem(TimeSeries):
    """
    The MDSystem class will regroup all functionalities of TOCATA-MD in a single class.
    """

    def __init__(self):
        """
        Initialize a complete set of all essential system parameters :
            - sysdir: the directory (ABSOLUTE path) containing some or all files of the system to analyze.
            - input: input trajectory files (multiple, handles wildcards).
            - trajs: processed trajectory files
            - topolFile: topology file
            - topol: system topology (.gro, .pdb).
            - tpr: structure file (.tpr) to automatize post-analysis structure output.
            - featureFile: features to be analyzed.
            - timeStep: time between each frame in each trajectory (must be uniform for all trajectories or all results will be worthless).
            - totalLength: total trajectory length, in number of frames.
            - output: common prefix for all output files.

        Future ideas :
        - Cyclostationary analysis (to implement with delay) :
            Ho KC, Hamelberg D. Oscillatory Diffusion and Second-Order Cyclostationarity in Alanine Tripeptide from Molecular Dynamics Simulation. 2015. JCTC. Vol 12. p372-382
        - Alternative implementation of cyclostationary analysis :
            - Look for change of sign in cross-correlation with time
            - Take into account the Nyquist rate for frequency resolution
            - To identify optimal lagtimes, use autocorrelation
        
        IMPORTANT : When dealing with a full-atom system (i.e. solvent, membranes), it is imperative that the analysis only includes the protein and its ligands.
        This program is not designed to explore protein-solvent or protein-membrane interactions. In order to only analyze the protein and its ligands, the following
        nomenclature should be adhered to in order for the program to fully function proprely:
        - Using gromacs, create the protein-ligand-only version of the system (.xtc) by selecting the appropriate selection group. The name of the trajectory should
        be extended with "_prot.xtc" (e.g. system.xtc -> system_prot.xtc). If you do not create simulation restarts, it is not necessary to comply with this nomenclature.  
        """

        TimeSeries.__init__(self)
        self.sysDir = None
        self.input = []
        self.topolFile = None
        self.topol = None
        self.tpr = None
        self.featureFile = None
        self.textFeatures = {"distances" : None,
                             "angles" : None,
                             "dihedrals" : None,
                             "dihedralClasses" : None,
                             "exclude" : None,
                             "keep" : None,
                             "nochain" : False}
        self.timeStep = None # Time between frames in ps (10^-12)
        self.totalTimeLength = None # Total trajectory length in µs (10^-6)

        # Log tracker for system essentials
        self.logStatus = {"commonInput" : False, "features" : False, "featureAtoms" : False, "trajs" : False}

        self.featToSeq = None
        self.seqToFeat = None

    def initializeFromCommandLine(self) -> None:
        """
        Initialize a complete set of all essential system parameters :
            - sysdir: the directory (ABSOLUTE path) containing some or all files of the system to analyze.
            - input: input trajectory files (multiple, handles wildcards).
            - trajs: processed trajectory files
            - topolFile: topology file
            - topol: system topology (.gro, .pdb).
            - tpr: structure file (.tpr) to automatize post-analysis structure output.
            - featureFile: features to be analyzed.
            - timeStep: time between each frame in each trajectory (must be uniform for all trajectories or all results will be worthless).
            - totalLength: total trajectory length, in number of frames.
            - output: common prefix for all output files.
        """

        parser = ArgumentParser()
        parser.add_argument("-d", "--sysdir") # Path to system directory (optional)
        parser.add_argument("-i", "--input", nargs='*') # All input trajectories
        # trajs will be initialized as empty list and filled later
        parser.add_argument("-p", "--topol") # System topology
        parser.add_argument("-s", "--tpr") # tpr file to automatize structure output
        parser.add_argument("-f", "--features") # Feature file
        parser.add_argument("-t", "--timestep") # Time between frames in ps
        # totalLength is not defined until trajectories are loaded
        parser.add_argument("-o", "--output") # Output prefix

        argDict = vars(parser.parse_args())

        # sysDir must exist if specified
        self.sysDir = argDict["sysdir"]
        if not self.sysDir is None:
            if not os.path.isdir(self.sysDir):
                raise Exception("Directory {:s} does not exist.".format(self.sysDir))
            else:
                self.sysDir = os.path.abspath(self.sysDir)

            self.sysDir += "/"

        self.input = argDict["input"]

        for i in range(len(self.input)):
            if re.search(r"\*", self.input[i]):
                beg = self.input[:i]
                nex = self.input[i+1:]
                ele = self.input[i]

                expansion = glob.glob(ele)

                if not expansion:
                    expansion = glob.glob(self.sysDir + ele)

                    if not expansion:
                        raise Exception("Input {:s} does not match anything.".format(ele))
                    
                    for i in range(len(expansion)):
                        expansion[i] = re.sub(self.sysDir, "", expansion[i])

                beg.extend(expansion)
                beg.extend(nex)

                self.input = beg

        #print(self.input)

        self.tpr = argDict["tpr"]

        # Immediately load system topology as an MDTraj topology object
        self.topolFile = argDict["topol"]
        if not self.topolFile is None:
            if self.topolFile != "pdb":
                self.solveLocation(self.tpr)
                self.topol = md.load(self.solveLocation(self.topolFile)).topology

        self.featureFile = argDict["features"]

        self.loadFeatures()

        self.timeStep = float(argDict["timestep"])

        self.totalLength = 0.0
        
        self.output = argDict["output"]

    def loadFeatures(self) -> None:
        """
        This function loads feature files

        Feature files have the following format:
        
        Distances
        chainid1:resname1:resSeq1:name1,chainid2:resname2:resSeq2:name2
        ...
        
        Angles
        chainid1:resname1:resSeq1:name1,chainid2:resname2:resSeq2:name2,chainid3:resname3:resSeq3:name3
        ...
        
        Dihedrals
        chainid1:resname1:resSeq1:name1,chainid2:resname2:resSeq2:name2,chainid3:resname3:resSeq3:name3,chainid4:resname4:resSeq4:name4
        
        Dihedral classes
        phi
        psi
        chi1
        ...
        chi4
        
        Include
        selection string
        
        Exclude
        selection string
        
        No chain (optional)

        Dihedral classes can include any number of the above-mentioned parameters.
        For all specific measurements, if the No chain feature is specified, each chainid is an empty string (e.g. ":ALA:1:CA")

        This feature file syntax is based on the corresponding names of the mdtraj selection DSL :
        http://mdtraj.org/latest/atom_selection.html
        http://mdtraj.org/latest/api/generated/mdtraj.Topology.html
        """
        
        feat_file = open(self.solveLocation(self.featureFile), 'r')
    
        current = ""
    
        for line in feat_file:
            print(line.strip('\n'))
    
            if re.match("Distances", line):
                current = "distances"
                continue
            if re.match("Angles", line):
                current = "angles"
                continue
            if re.match("Dihedrals", line):
                current = "dihedrals"
                continue
            if re.match("Dihedral classes", line):
                current = "dihedralClasses"
                continue
            if re.match("Exclude", line):
                current = "exclude"
                continue
            if re.match("Keep", line):
                current = "keep"
                continue
            if re.match("No chain", line):
                self.textFeatures["nochain"] = True
                continue
                
            if current == "distances":
                if self.textFeatures["distances"] is None:
                    self.textFeatures["distances"] = [line.rstrip()]
                else:
                    self.textFeatures["distances"].append(line.rstrip())
                continue
            if current == "angles":
                if self.textFeatures["angles"] is None:
                    self.textFeatures["angles"] = [line.rstrip()]
                else:
                    self.textFeatures["angles"].append(line.rstrip())
                continue
            if current == "dihedrals":
                if self.textFeatures["dihedrals"] is None:
                    self.textFeatures["dihedrals"] = [line.rstrip()]
                else:
                    self.textFeatures["dihedrals"].append(line.rstrip())
                continue
            if current == "dihedralClasses":
                if self.textFeatures["dihedralClasses"] is None:
                    self.textFeatures["dihedralClasses"] = [line.rstrip()]
                else:
                    self.textFeatures["dihedralClasses"].append(line.rstrip())
                continue
            if current == "exclude":
                self.textFeatures["exclude"] = "not ({:s})".format(line.rstrip())
                continue
            if current == "keep":
                self.textFeatures["keep"] = line.rstrip()

        self.trimmedTopol = self.topol

        if not (self.textFeatures["keep"] is None):
            self.trimmedTopol = self.trimmedTopol.subset(self.topol.select(self.textFeatures["keep"]))
        if not (self.textFeatures["exclude"] is None):
            self.trimmedTopol = self.trimmedTopol.subset(self.topol.select(self.textFeatures["exclude"]))

        #top,dum = self.trimmedTopol.to_dataframe()

        #print(top)

    def initializeFromSQL(self, pathToSQL: str) -> None:
        """
        TODO : Find the bug in trajectory loading from SQL that produces a wrong trimming
        For now, all systems where only a part of it is analyzed should be done by command line
        Load all common system parameters from database:
            - sysdir: the directory (ABSOLUTE path) containing some or all files of the system to analyze.
            - input: input trajectory files (multiple, handles wildcards).
            - topol: system topology (.gro, .pdb), as a pandas dataframe.
            - tpr: structure file (.tpr) to automatize post-analysis structure output.
            - featureFile: features to be analyzed.
            - timeStep: time between each frame in each trajectory (must be uniform for all trajectories or all results will be worthless).
            - output: common prefix for all output files.

        Keep in mind that the path handling of the MDSystem class is very rigid by design.
        The path handling philosophy is: one system, one data directory.
        Two directories is pushing it, three is unacceptable. Please keep your files tidy.
        """

        self.setDBPath(pathToSQL)

        conn = sqlite3.connect(self.dbPath)
        curs = conn.cursor()

        curs.execute("SELECT * FROM common_input WHERE name = 'sysDir'")

        getter = curs.fetchall()

        # sysDir is mutable only if the actual system directory has been moved. One system, one directory.
        if getter:
            self.sysDir = getter[0][2]

        # Inputs are immutable: changing the input means changing the system.
        curs.execute("SELECT * FROM common_input WHERE name = 'input'")

        getter = curs.fetchall()

        self.input = []

        if getter:
            for i in range(len(getter)):
                self.input.append("")

            for ele in getter:
                self.input[ele[1]] = ele[2]

        # Topology is immutable for the same reason
        table = pd.read_sql("SELECT * FROM topology_atoms", conn)
        table.drop(labels="index",axis=1)       
        self.topol = md.Topology().from_dataframe(table)

        curs.execute("SELECT * FROM common_input WHERE name = 'tpr'")

        getter = curs.fetchall()

        if getter:
            self.tpr = getter[0][2]
        
        # featureFile is immutable
        curs.execute("SELECT * FROM common_input WHERE name = 'featureFile'")

        getter = curs.fetchall()

        if getter:
            self.featureFile = getter[0][2]

        curs.execute("SELECT * FROM common_input WHERE name = 'timeStep'")

        getter = curs.fetchall()

        if getter:
            self.timeStep = getter[0][2]

        curs.execute("SELECT * FROM common_input WHERE name = 'output'")

        getter = curs.fetchall()

        if getter:
            self.output = getter[0][2]

        curs.execute("SELECT * FROM text_parameters")

        getter = curs.fetchall()

        for ele in getter:
            #print(ele[0])
            #print(self.textFeatures[ele[0]])
            if not ele[1] is None:
                if self.textFeatures[ele[0]] is None:
                    self.textFeatures[ele[0]] = [""]
                else:
                    self.textFeatures[ele[0]].append("")
            else:
                self.textFeatures[ele[0]] = ele[2]

        for ele in getter:
            if not ele[1] is None:
                self.textFeatures[ele[0]][ele[1]] = ele[2]

        self.textFeatures["nochain"] = bool(self.textFeatures["nochain"])

        self.trimmedTopol = self.topol

        if not (self.textFeatures["keep"] is None):
            self.trimmedTopol = self.trimmedTopol.subset(self.topol.select(self.textFeatures["keep"]))
        if not (self.textFeatures["exclude"] is None):
            self.trimmedTopol = self.trimmedTopol.subset(self.topol.select(self.textFeatures["exclude"]))

        #top,dum = self.trimmedTopol.to_dataframe()

        #top)

        conn.close()
    
    def loadTopolFromFile(self,path):
        self.topol = md.load(self.solveLocation(path)).topology

        self.trimTopol()

    def trimTopol(self):
        self.trimmedTopol = self.topol

        if not (self.textFeatures["keep"] is None) and self.topol is not None:
            self.trimmedTopol = self.trimmedTopol.subset(self.topol.select(self.textFeatures["keep"]))
        if not (self.textFeatures["exclude"] is None) and self.topol is not None:
            self.trimmedTopol = self.trimmedTopol.subset(self.topol.select(self.textFeatures["exclude"]))

    def solveLocation(self, fileName: str) -> str:
        """
        Return the actual location of a file.
        """
        #print(self.sysDir + fileName)
        if os.path.isfile(fileName):
            return fileName
        elif not (self.sysDir is None) and os.path.isfile(self.sysDir + fileName):
            return self.sysDir + fileName
        else:
            raise Exception("File {:s} does not exist in current working directory or specified system directory {}".format(fileName, self.sysDir))

    def controlCommonInput(self) -> None:
        """
        Control system input files and essential parameters:
            - input must contain existing files if trajectory features are not already computed,
            - topology must exist as an md.Topology instance,
            - tpr file must exist,
            - feature file must exist if features are not already logged,
            - timeStep must be defined and valid,
            - output prefix must be defined and contain only letters, numbers and underscores.
        """
        
        if not self.trajs:
            for trajin in self.input:
                self.solveLocation(trajin)

        if not isinstance(self.topol, md.Topology):
            raise Exception("System topology is not correctly defined.")

        self.solveLocation(self.tpr)
        
        if self.features is None:
            self.solveLocation(self.featureFile)
            
        if self.timeStep <= 0.0 or self.timeStep is None:
            raise Exception("The system must have a defined and valid time between each of its MD frames.")
        
        if self.output is None or not re.match(r"^\w+$", self.output):
            raise Exception("The system output prefix must be defined, contain at least one symbol and only contain letters, numbers and underscores.")

    def logInit(self, overwrite=False) -> None:
        """
        Log all common system parameters :
            - input: input trajectory files (multiple, handles wildcards).
            - topolFile: system topology file
            - topol: system topology (.gro, .pdb), as a pandas dataframe.
            - tpr: structure file (.tpr) to automatize post-analysis structure output.
            - features
            - timeStep: time between each frame in each trajectory (must be uniform for all trajectories or all results will be worthless).
            - output: common prefix for all output files.

        Keep in mind that the path handling of the MDSystem class is very rigid by design.
        The path handling philosophy is: one system, one data directory.
        Two directories is pushing it, three is unacceptable. Please keep your files tidy.
        """

        conn = sqlite3.connect(self.dbPath)
        
        if(overwrite):
            tables = self.dbTables(conn)

            if "common_input" in tables:
                conn.execute("DROP TABLE common_input")
            if "text_parameters" in tables:
                conn.execute("DROP TABLE text_parameters")
            if "topology_atoms" in tables:
                conn.execute("DROP TABLE topology_atoms")

        conn.execute("CREATE TABLE common_input (name TEXT, i UNSIGNED INTEGER, value BLOB)")

        if not self.sysDir is None:
            conn.execute("INSERT INTO common_input VALUES (?,NULL,?)", ("sysDir", self.sysDir))

        for i in range(len(self.input)):
            conn.execute("INSERT INTO common_input VALUES (?,?,?)", ("input", i, self.input[i]))

        conn.execute("INSERT INTO common_input VALUES (?,NULL,?)",("tpr", self.tpr))
        conn.execute("INSERT INTO common_input VALUES (?,NULL,?)",("featureFile", self.featureFile))
        conn.execute("INSERT INTO common_input VALUES (?,NULL,?)",("timeStep", self.timeStep))
        conn.execute("INSERT INTO common_input VALUES (?,NULL,?)",("output", self.output))

        conn.execute("CREATE TABLE text_parameters (category TEXT, i UNSIGNED INTEGER, selstr CLOB(200))")

        for cat in self.textFeatures.keys():
            if type(self.textFeatures[cat]) is list:
                for i in range(len(self.textFeatures[cat])):
                    conn.execute("INSERT INTO text_parameters VALUES (?,?,?)", (cat,i,self.textFeatures[cat][i]))
            elif type(self.textFeatures[cat]) is bool:
                conn.execute("INSERT INTO text_parameters VALUES (?,?,?)", (cat,None,int(self.textFeatures[cat])))
            elif type(self.textFeatures[cat]) is str:
                conn.execute("INSERT INTO text_parameters VALUES (?,?,?)", (cat,None,self.textFeatures[cat]))

        if not self.topol is None:
            # Log topology
            table, bonds = self.topol.to_dataframe()
            table.to_sql("topology_atoms", conn)

        conn.commit()
        conn.close()

    def loadTrajs(self, fullDihedralClasses=False) -> None:
        """
        Calculate all trajectory features contained in self.features, for all trajectories contained in self.trajs.
        """
        
        self.preLoadTrajs()
        
        print("Computing features...")

        self.trajs = []
        self.featureOrder = []
        self.featureLengths = []
        fillOrder = True

        for pretraj in self.rawTrajs:
            print(pretraj)
            features = None

            """
            This section is now greatly simplified.
            self.features now contains all system atom numbers related to each feature type.
            This will have to be taken into account when doing tICA analysis.
            """
            
            for selType in self.textFeatures.keys():
                if not (self.textFeatures[selType] is None) and (selType != "dihedralClasses" and selType != "include" and selType != "exclude" and selType != "nochain"):
                    typeSels, typeFeats = self.calcFeatures(pretraj, self.textFeatures[selType], selType, self.textFeatures["nochain"])

                    if features is None:
                        features = typeFeats
                    else:
                        features = np.append(features, typeFeats, axis=1)

                    self.features[selType] = typeSels
                    if fillOrder:
                        self.featureOrder.append(selType)
                        self.featureLengths.append(len(self.textFeatures[selType]))

            # .. # Extract all specified dihedral classes (phi, psi, chi1, chi2, chi3, chi4) from trajectory
            
            if not (self.textFeatures["dihedralClasses"] is None):
                dihClassAtoms, dihClassFeats, lengths = self.calcDihedralClasses(pretraj, self.textFeatures["dihedralClasses"],fullDihedralClasses)
                
                if features is None:
                    features = dihClassFeats
                else:
                    features = np.append(features, dihClassFeats, axis=1)
                    
                self.features["dihedralClasses"] = dihClassAtoms
                if fillOrder:
                    self.featureOrder.append("dihedralClasses")
                    self.featureLengths.extend(lengths)
            
            self.trajs.append(features)
            fillOrder = False
        
        for traj in self.trajs:
            tmp = len(traj)

            self.totalLength += tmp

            if tmp < self.maxLag:
                self.maxLag = tmp

        print(self.maxLag)
        self.maxLag = self.maxLag >> 1

        self.totalTimeLength = self.totalLength * self.timeStep / 1000000.0

    def preLoadTrajs(self):
        self.rawTrajs = []

        print(self.textFeatures)

        self.totalLength = 0
    
        for trajin in self.input:
            print("Loading {:s}".format(trajin))
            
            # Load raw information from each trajectory
            # !! # The "topology" (the .gro file) must be included if the trajectory comes from GROMACS
            traj1 = None
            if self.topol is None:
                traj1 = md.load(self.solveLocation(trajin))
                self.topol = traj1.topology
                self.trimTopol()
            else:
                traj1 = md.load(self.solveLocation(trajin), top=self.topol)
            
            print(traj1)
            print(traj1[0])
            self.rawTrajs.append(traj1)
            
        # Remove all excluded and keep only the "keep" section
        
        print("Trimming trajectories...")
        
        for i in range(len(self.rawTrajs)):
            if not (self.textFeatures["keep"] is None):
                self.rawTrajs[i].atom_slice(self.topol.select(self.textFeatures["keep"]), inplace=True)

            if not (self.textFeatures["exclude"] is None):
                self.rawTrajs[i].atom_slice(self.topol.select(self.textFeatures["exclude"]), inplace=True)

    def calcFeatures(self, trajin, sels, feat_type, nochain):
        """
            Calculates trajectory features
            
            trajin: trajectory from which features will be extracted
            sels: selection strings from the feature file
            feat_type: distance, angle or dihedral

            This function uses mdtraj to compute features.
        """

        groups = []

        if not (feat_type == "distances" or feat_type == "angles" or feat_type == "dihedrals"):
            raise Exception("Could not understand feature type \"{:s}\" in calc_features.".format(feat_type))

        # Parse feature selection strings
        for sel in sels:
            # Get individual atom informations as a list instead of a single string
            sel = sel.split(r",")

            # Check for consistency between feat_type and number of atoms in selection string.
            if feat_type == "distances" and len(sel) != 2:
                raise Exception("Distance feature type does not require {:d} atoms, but 2.".format(len(sel)))
            if feat_type == "angles" and len(sel) != 3:
                raise Exception("Angle feature type does not require {:d} atoms, but 3.".format(len(sel)))
            if feat_type == "dihedrals" and len(sel) != 4:
                raise Exception("Dihedral feature type does not require {:d} atoms, but 4.".format(len(sel)))
            
            # Fill out a mdtraj-compatible selection string for the given feature.
            sel_str = ""
            for atm in sel:
                # Account for different selection formats in case of nochain
                if nochain:
                    match = re.match(r":(\w+):(\-?\d+):(\w+)",atm)
                    if not match:
                        raise Exception("Could not parse atom information ({:s}) in a no-chain context".format(atm))

                    if not sel_str:
                        sel_str = "(resname {:s} and resSeq {:s} and name {:s})".format(match.group(1), match.group(2), match.group(3))
                    else:
                        sel_str = sel_str + " or (resname {:s} and resSeq {:s} and name {:s})".format(match.group(1), match.group(2), match.group(3))
                else:
                    match = re.match(r"(\-?\d+):(\w+):(\-?\d+):(\w+)",atm)
                    if not match:
                        raise Exception("Could not parse atom information ({:s}) in a with-chain context".format(atm))

                    if not sel_str:
                        sel_str = "(chain {:s} and resname {:s} and resSeq {:s} and name {:s})".format(match.group(1), match.group(2), match.group(3), match.group(4))
                    else:
                        sel_str = sel_str + " or (chain {:s} and resname {:s} and resSeq {:s} and name {:s})".format(match.group(1), match.group(2), match.group(3), match.group(4))

            # Append mdtraj atom numbers to feature groups
            try:
                groups.append(trajin.top.select(sel_str))
            except:
                raise Exception("Could not parse selection : {:s}".format(sel_str))

        groups = np.array(groups)

        if feat_type == "distances":
            return groups, md.compute_distances(trajin, groups)
        if feat_type == "angles":
            # Compute angle cos and sin to give complete picture and remove periodicity artifacts
            ang = md.compute_angles(trajin, groups)

            ang = np.append(np.cos(ang), np.sin(ang), axis=1)

            return groups, ang
        if feat_type == "dihedrals":
            # Same as for angle
            dih = md.compute_dihedrals(trajin, groups)

            dih = np.append(np.cos(dih), np.sin(dih), axis=1)

            return groups, dih

    def calcDihedralClasses(self,trajin, dihedralClasses, fullDihedralClasses):
        """
        Compute and transform requested dihedral classes

        Phi:
        Cos is used because optimal phi angles for amino acids are located in the 0-180° range
        (the maximum derivative of cos is at 90°, meaning it separates the angles better around this region)

        For info, Sin has its minimal derivative at 90°, meaning it is terrible to separate phi angles.

        It is important to note that using a periodic function is essential to account for angle periodicity.
        Otherwise, angles 1° and 359° would be separated by a wide margin.

        Psi:
        Same as for phi, but this time, favored regions for psi angles are located around 0° and 180°,
        meaning that cos is terrible to separate psi angles.

        Chi1-4:
        Because it is impossible to consider all sorts of specific side chain dihedral conformations, it is assumed
        that prefered sidechain dihedrals resemble butane's.
        This means that prefered regions are 0° and ±120°, once again best covered by sin
        
        It is also not computationally efficient to get both sin and cos of dihedral classes, given the large
        amount of these in a protein and the subsequent O(n^2) to O(n^3) matrix operations done on these features.
        If the full picture of all dihedrals and dihedral classes is truly required, then its implementation is trivial 
        and could be done super fast.

        Reference for ramachandran-favored regions : Laskowski RA et al. J App Cryst. 1993. Vol 26. p282-291
        """

        top, dum = self.trimmedTopol.to_dataframe()

        dihClassFeats = None
        dihClassAtoms = None
        lengths = []
        self.allChi2 = None
        self.allChi3 = None
        
        for dihedralClass in dihedralClasses:
            #print("Loading {:s} angles...".format(dihedral_class))
            
            if dihedralClass == "phi":
                phiAtoms, phi = md.compute_phi(trajin) # Phi is computed
                
                lengths.append(len(phi[0]))

                if dihClassFeats is None:
                    if fullDihedralClasses:
                        dihClassFeats = phi
                    else:    
                        dihClassFeats = np.cos(phi)
                    dihClassAtoms = phiAtoms
                else:
                    if fullDihedralClasses:
                        dihClassFeats = np.append(dihClassFeats, phi, axis=1)
                    else:    
                        dihClassFeats = np.append(dihClassFeats, np.cos(phi), axis=1)
                    dihClassAtoms = np.append(dihClassAtoms, phiAtoms, axis=0)
            elif dihedralClass == "psi":
                psiAtoms, psi = md.compute_psi(trajin)
                
                lengths.append(len(psi[0]))

                if dihClassFeats is None:
                    if fullDihedralClasses:
                        dihClassFeats = psi
                    else:    
                        dihClassFeats = np.sin(psi)
                    dihClassAtoms = psiAtoms
                else:
                    if fullDihedralClasses:
                        dihClassFeats = np.append(dihClassFeats, psi, axis=1)
                    else:    
                        dihClassFeats = np.append(dihClassFeats, np.sin(psi), axis=1)
                    dihClassAtoms = np.append(dihClassAtoms, psiAtoms, axis=0)
            elif dihedralClass == "chi1":
                chi1Atoms, chi1 = md.compute_chi1(trajin)

                lengths.append(len(chi1[0]))
                
                if dihClassFeats is None:
                    if fullDihedralClasses:
                        dihClassFeats = chi1
                    else:    
                        dihClassFeats = np.sin(chi1)
                    dihClassAtoms = chi1Atoms
                else:
                    if fullDihedralClasses:
                        dihClassFeats = np.append(dihClassFeats, chi1, axis=1)
                    else:    
                        dihClassFeats = np.append(dihClassFeats, np.sin(chi1), axis=1)
                    dihClassAtoms = np.append(dihClassAtoms, chi1Atoms, axis=0)
            elif dihedralClass == "chi2":
                chi2Atoms, chi2 = md.compute_chi2(trajin)

                lengths.append(len(chi2[0]))
                
                if self.allChi2 is None:
                    self.allChi2 = top.loc[top["resName"].str.contains(r"ARG|ASN|ASP|GLN|GLU|HIS|ILE|LEU|LYS|MET|PHE|PRO|TRP|TYR")].drop_duplicates(subset=["resSeq",
                    "chainID"])["resName"].tolist()

                print("Enacting chi2 symmetry for ASP, PHE and TYR...")

                if fullDihedralClasses:
                    for i in range(len(self.allChi2)):
                        if self.allChi2[i] == "ASP" or self.allChi2[i] == "PHE" or self.allChi2[i] == "TYR":
                            for j in range(len(chi2)):
                                if chi2[j][i] < 0:
                                    chi2[j][i] += math.pi
                else:
                    chi2 = np.sin(chi2)

                    for i in range(len(self.allChi2)):
                        if self.allChi2[i] == "ASP" or self.allChi2[i] == "PHE" or self.allChi2[i] == "TYR":
                            for j in range(len(chi2)):
                                chi2[j][i] *= chi2[j][i]

                if dihClassFeats is None:
                    dihClassFeats = chi2
                    dihClassAtoms = chi2Atoms
                else:
                    dihClassFeats = np.append(dihClassFeats, chi2, axis=1)
                    dihClassAtoms = np.append(dihClassAtoms, chi2Atoms, axis=0)
            elif dihedralClass == "chi3":
                chi3Atoms, chi3 = md.compute_chi3(trajin)

                lengths.append(len(chi3[0]))
                
                if self.allChi3 is None:
                    self.allChi3 = top.loc[top["resName"].str.contains(r"ARG|GLN|GLU|LYS|MET")].drop_duplicates(subset=["resSeq",
                    "chainID"])["resName"].tolist()

                print("Enacting chi3 symmetry for GLU...")

                if fullDihedralClasses:
                    for i in range(len(self.allChi3)):
                        if self.allChi3[i] == "GLU":
                            for j in range(len(chi3)):
                                if chi3[j][i] < 0:
                                    chi3[j][i] += math.pi
                else:
                    chi3 = np.sin(chi3)

                    for i in range(len(self.allChi3)):
                        if self.allChi3[i] == "GLU":
                            for j in range(len(chi3)):
                                chi3[j][i] *= chi3[j][i]

                if dihClassFeats is None:
                    dihClassFeats = chi3
                    dihClassAtoms = chi3Atoms
                else:
                    dihClassFeats = np.append(dihClassFeats, chi3, axis=1)
                    dihClassAtoms = np.append(dihClassAtoms, chi3Atoms, axis=0)
            elif dihedralClass == "chi4":
                chi4Atoms, chi4 = md.compute_chi4(trajin)

                lengths.append(len(chi4[0]))
                
                if dihClassFeats is None:
                    if fullDihedralClasses:
                        dihClassFeats = chi4
                    else:    
                        dihClassFeats = np.sin(chi4)
                    dihClassAtoms = chi4Atoms
                else:
                    if fullDihedralClasses:
                        dihClassFeats = np.append(dihClassFeats, chi4, axis=1)
                    else:
                        dihClassFeats = np.append(dihClassFeats, np.sin(chi4), axis=1)
                    dihClassAtoms = np.append(dihClassAtoms, chi4Atoms, axis=0)
            else:
                raise Exception("Feature format \"{:s}\" is invalid in a dihedral class measurement context.".format(dihedralClass))
        
        return dihClassAtoms, dihClassFeats, lengths

    def logFeatureAtoms(self, overwrite=False) -> None:
        """
        Log trajectory atoms for later use.
        """

        conn = sqlite3.connect(self.dbPath)

        if overwrite:
            tables = self.dbTables(conn)

            if "feature_atoms" in tables:
                conn.execute("DROP TABLE feature_atoms")
            if "feature_order" in tables:
                conn.execute("DROP TABLE feature_order")

        conn.execute("CREATE TABLE feature_order (i UNSIGNED INTEGER, category TEXT)")
        conn.execute("CREATE TABLE feature_atoms (category TEXT, i UNSIGNED INTEGER, atom1 INTEGER, atom2 INTEGER, atom3 INTEGER, atom4 INTEGER)")

        for i in range(len(self.featureOrder)):
            cat = self.featureOrder[i]

            conn.execute("INSERT INTO feature_order VALUES (?,?)", (i, cat))
            
            feats = self.features[cat]

            if cat == "distances":
                for j in range(len(feats)):
                    conn.execute("INSERT INTO feature_atoms VALUES (?,?,?,?,NULL,NULL)", (cat,j,int(feats[j][0]),int(feats[j][1])))
            elif cat == "distances":
                for j in range(len(feats)):
                    conn.execute("INSERT INTO feature_atoms VALUES (?,?,?,?,?,NULL)", (cat,j,int(feats[j][0]),int(feats[j][1]),int(feats[j][2])))
            else:
                for j in range(len(feats)):
                    conn.execute("INSERT INTO feature_atoms VALUES (?,?,?,?,?,?)", (cat,j,int(feats[j][0]),int(feats[j][1]),int(feats[j][2]),int(feats[j][3])))

        conn.commit()
        conn.close()

    def loadFeatureAtomsFromSQL(self) -> None:
        """
        Log trajectory atoms for later use.
        """

        conn = sqlite3.connect(self.dbPath)
        curs = conn.cursor()

        curs.execute("SELECT * FROM feature_order")
        table = curs.fetchall()

        #print(table)

        self.featureOrder = list((0,)*len(table))

        for ele in table:
            self.featureOrder[ele[0]] = ele[1]

        #print(self.featureOrder)

        for i in range(len(self.featureOrder)):
            # This is terrible, but sqlite does not understand otherwise
            if self.featureOrder == "distances":
                curs.execute("SELECT * FROM feature_atoms WHERE category = 'distances'")
                table = curs.fetchall()

                self.features["distances"] = np.zeros((len(table),2),dtype=int)

                tmp = self.features["distances"]

                for ele in table:
                    tmp[ele[1]][0] = ele[2]
                    tmp[ele[1]][1] = ele[3]
            elif self.featureOrder[i] == "angles":
                curs.execute("SELECT * FROM feature_atoms WHERE category = 'angles'")
                table = curs.fetchall()

                self.features["angles"] = np.zeros((len(table),3),dtype=int)

                tmp = self.features["angles"]

                for ele in table:
                    tmp[ele[1]][0] = ele[2]
                    tmp[ele[1]][1] = ele[3]
                    tmp[ele[1]][2] = ele[4]
            else:
                if self.featureOrder[i] == "dihedrals":
                    curs.execute("SELECT * FROM feature_atoms WHERE category = 'dihedrals'")
                else:
                    curs.execute("SELECT * FROM feature_atoms WHERE category = 'dihedralClasses'")
                
                table = curs.fetchall()

                self.features[self.featureOrder[i]] = np.zeros((len(table),4),dtype=int)

                tmp = self.features[self.featureOrder[i]]

                for ele in table:
                    tmp[ele[1]][0] = ele[2]
                    tmp[ele[1]][1] = ele[3]
                    tmp[ele[1]][2] = ele[4]
                    tmp[ele[1]][3] = ele[5]

    def featureToSequence(self) -> None:
        """
        Creates a sequence of chain/resSeq pairs contained in a given feature.
        This sequence follow the feature order.
        Diheral angles only include the residue to which they apply.
        """
        
        print("Calculating feature-to-sequence information...")
        topol,dum = self.topol.to_dataframe()

        #print(topol.to_string())

        self.featToSeq = []
        for order in self.featureOrder:
            featureAtoms = self.features[order]

            nAtoms = len(featureAtoms[0])

            if order != "dihedralClasses":
                for i in range(len(featureAtoms)):
                    tmp = []
                    for j in range(nAtoms):
                        
                        res = vars(self.topol.atom(featureAtoms[i][j]))["residue"]

                        chainID = res.chain.index
                        resSeq = res.index

                        isNew = True

                        for k in range(len(tmp)):
                            if tmp[k][0] == chainID and tmp[k][1] == resSeq:
                                isNew = False
                                break

                        if isNew:
                            tmp.append((chainID, resSeq))

                    self.featToSeq.append(tmp)
            else:
                for i in range(len(featureAtoms)):
                    tmp = []
                    # Atoms 0 and 3 can belong to other residues.
                    res1 = self.topol.atom(featureAtoms[i][1]).residue
                    atm1 = (res1.chain.index, res1.index)
                    res2 = self.topol.atom(featureAtoms[i][2]).residue
                    atm2 = (res2.chain.index, res2.index)
                    res3 = self.topol.atom(featureAtoms[i][3]).residue
                    atm3 = (res3.chain.index, res3.index)

                    if atm1[0] == atm2[0] and atm1[1] != atm2[1] and atm1[0] == atm3[0] and atm1[1] != atm3[1]:
                        tmp.append(atm2)
                    else:
                        tmp.append(atm1)

                    self.featToSeq.append(tmp)
    
    def sequenceToFeatures(self) -> None:
        """
        Creates a dictionary associating chain ID and resSeq to features.
        It is the reverse process of featureToSequence, and must be done after it.
        """

        if (not self.featToSeq) or (self.featToSeq is None):
            self.featureToSequence()

        print("Calculating sequence-to-features information...")

        self.seqToFeat = dict()

        for i in range(len(self.featToSeq)):
            for j in range(len(self.featToSeq[i])):
                chainID = self.featToSeq[i][j][0]
                resSeq = self.featToSeq[i][j][1]

                if not chainID in self.seqToFeat:
                    self.seqToFeat[chainID] = dict()

                if not resSeq in self.seqToFeat[chainID]:
                    self.seqToFeat[chainID][resSeq] = []
                
                self.seqToFeat[chainID][resSeq].append(i)

    def binDihedrals(self,nBins:int) -> list:
        self.preLoadTrajs()

        first = True
        allFrames = None
        lengths = []
        dihClassAtoms = []

        for pretraj in self.rawTrajs:
            dihClassAtoms, dihClassFeats, lengths = self.calcDihedralClasses(pretraj, self.textFeatures["dihedralClasses"],True)
            if first:
                allFrames = dihClassFeats
                first = False
            else:
                allFrames = np.append(allFrames,dihClassFeats,axis=0)

        self.rawTrajs = None

        allFrames = np.transpose(allFrames)

        first = True
        bins = list(range(1,nBins+1))
        for i in range(len(bins)):
            bins[i] = (2*bins[i] - 1)*math.pi/nBins
        filledBins = None
        binEdges = None
        valuesRange = (-math.pi-0.001,math.pi+0.001)

        for i in range(len(allFrames)):
            if first:
                filledBins = np.histogram(allFrames[i],nBins,valuesRange,density=True)
                binEdges = filledBins[1]
                filledBins = np.array([filledBins[0]])
                first = False
            else:
                histo = np.histogram(allFrames[i],nBins,valuesRange,density=True)
                histo = np.array([histo[0]])
                filledBins = np.append(filledBins,histo,axis=0)
        
        bins = np.array(bins) - math.pi

        for i in range(1,len(lengths)):
            lengths[i] += lengths[i-1]
        
        return bins, filledBins, dihClassAtoms, lengths

    def tICASequenceScan(self) -> None:
        """
        Perform a tICA scan on all residues

        sequenceScans structure :
        first dimension : each different chain scan
        second dimension : 0 - resSeq (not necessarily in order)
                           1 - evecAngles
                           2 - evalRatios
                           3 - resSeqLabels
                           4 - ChainID
        third dimension (if available) : eigenvector number.
        """

        print("Initiating tICA scan.")
        if not self.seqToFeat:
            self.sequenceToFeatures()

        topol,dum = self.topol.to_dataframe()

        self.sequenceScans = []
        
        indexI = 0
        for i in sorted(self.seqToFeat.keys()):
            print("Scanning chain {:n}".format(i))
            chain = self.seqToFeat[i]

            sequenceLength = len(chain.keys())

            self.sequenceScans.append((np.zeros(sequenceLength,dtype=int),np.zeros((self.tICA.nWorthwhileComps, sequenceLength)),
                                        np.zeros((self.tICA.nWorthwhileComps,sequenceLength)), ["" for i in range(sequenceLength)], i))

            indexJ = 0
            previousJ = -1
            for j in sorted(chain.keys()):
                res = self.topol.chain(i)._residues[j]

                print("Scanning residue {:s}:{:n}".format(res.name,res.resSeq))

                toRemove = self.seqToFeat[i][j]

                print(toRemove)
                
                # SANIC FAST
                # This is about 30X faster than the last implementation, and quite easy to implement, too
                toKeep, subEvals, subEvecs = self.genSymSubMatrixAnalysis(toRemove, self.tICA.autoCorrMat,self.tICA.covMat)
                
                evals = self.tICA.eVals
                reducedEvecs = np.take(self.tICA.eVecs,toKeep,axis=0)[:,:len(toKeep)]

                # Normalization of subEvecs
                subEvecs = np.transpose(subEvecs)
                for k in range(len(subEvecs)):
                    subEvecs[k] = subEvecs[k] / np.linalg.norm(subEvecs[k])

                # Normalization of reducedEvecs
                reducedEvecs = np.transpose(reducedEvecs)
                for k in range(len(reducedEvecs)):
                    reducedEvecs[k] = reducedEvecs[k] / np.linalg.norm(reducedEvecs[k])
                
                resSeq = self.sequenceScans[indexI][0]
                evecAngles = self.sequenceScans[indexI][1]
                evalRatios = self.sequenceScans[indexI][2]
                resLabels = self.sequenceScans[indexI][3]

                # subEvals and subEvecs are in reverse order
                for k in range(self.tICA.nWorthwhileComps):
                    lowerBound = evals[k+len(toRemove)]
                    resSeq[indexJ] = j
                    evecAngles[k][indexJ] = np.abs(np.real(np.dot(subEvecs[-(k+1)],reducedEvecs[k])))
                    evalRatios[k][indexJ] = (subEvals[-(k+1)] - lowerBound)/(evals[k] - lowerBound)
                    resLabels[indexJ] = "C{:n}:{:s} {:n}".format(res.chain.index,res.name,res.resSeq)
                
                indexJ += 1

    def tICASequenceScanToCSV(self):
        """
        As tICA scans are more free form than anything else that is logged in this suite, so a SQL database does not suit our needs.
        We need to use CSVs.
        Data format :
        ChainID, resSeq, label, type (angle, ratio), dim1Data, dim2Data, ..., dimNData
        """

        with open(self.output + "_scan_metadata.csv", "w") as wf:
            wf.write("chains,"+str(len(self.sequenceScans)))
            for i in range(len(self.sequenceScans)):
                wf.write("," + str(len(self.sequenceScans[i][0])))
            wf.write("\n")
            wf.write("nWorthwhileComps," + str(self.tICA.nWorthwhileComps))
            wf.close()

        with open(self.output + "_scan_data.csv", "w") as wf:
            for i in range(len(self.sequenceScans)):
                resSeqs = self.sequenceScans[i][0]
                angles = self.sequenceScans[i][1]
                ratios = self.sequenceScans[i][2]
                labels = self.sequenceScans[i][3]
                chainID = self.sequenceScans[i][4]
                for j in range(len(resSeqs)):
                    wf.write(str(chainID) + "," + str(resSeqs[j]) + "," + labels[j] + ",angle")
                    for k in range(self.tICA.nWorthwhileComps):
                        wf.write("," + str(angles[k][j]))
                    wf.write("\n")
                    wf.write(str(chainID) + "," + str(resSeqs[j]) + "," + labels[j] + ",ratio")
                    for k in range(self.tICA.nWorthwhileComps):
                        wf.write("," + str(ratios[k][j]))
                    wf.write("\n")

            

    def tICAFeatureScan(self):
        self.featureScan = (np.arange(len(self.tICA.autoCorrMat),dtype=int),
        np.zeros((self.tICA.nWorthwhileComps, len(self.tICA.autoCorrMat))), np.zeros((self.tICA.nWorthwhileComps,len(self.tICA.autoCorrMat))))
        for i in range(len(self.tICA.autoCorrMat)):
            print("Feature {:n}".format(i))

            toKeep, subEvals, subEvecs = self.genSymSubMatrixAnalysis([i], self.tICA.autoCorrMat,self.tICA.covMat)
                
            evals = self.tICA.eVals
            reducedEvecs = np.take(self.tICA.eVecs,toKeep,axis=0)[:,:len(toKeep)]

            # Normalization of subEvecs
            subEvecs = np.transpose(subEvecs)
            for k in range(len(subEvecs)):
                subEvecs[k] = subEvecs[k] / np.linalg.norm(subEvecs[k])

            # Normalization of reducedEvecs
            reducedEvecs = np.transpose(reducedEvecs)
            for k in range(len(reducedEvecs)):
                reducedEvecs[k] = reducedEvecs[k] / np.linalg.norm(reducedEvecs[k])
            
            evecAngles = self.featureScan[1]
            evalRatios = self.featureScan[2]

            # subEvalsand subEvecs are in reverse order
            for k in range(self.tICA.nWorthwhileComps):
                lowerBound = evals[k+1]
                evecAngles[k][i] = np.abs(np.real(np.dot(subEvecs[-(k+1)],reducedEvecs[k])))
                evalRatios[k][i] = (subEvals[-(k+1)] - lowerBound)/(evals[k] - lowerBound)

    def createCentroids(self,extraOptions=""):
        if (self.HMM.stateCentroids is None):
            raise Exception("System HMM must first be calculated or loaded in order to generate restarts")
        
        print(self.HMM.stateCentroids)
        for i in range(len(self.HMM.stateCentroids)):
            self.rawTrajs[self.HMM.stateCentroids[i][0][0]][self.HMM.stateCentroids[i][0][1]].save_pdb("{:s}_state_{:d}.pdb".format(self.output, i))

    def createRestarts(self, nRestarts:int):
        if (self.HMM.stateCentroids is None):
            raise Exception("System HMM must first be calculated or loaded in order to generate restarts")

        if nRestarts < 1:
            raise Exception("Invalid number of restarts : {:n}".format(nRestarts))

        if (self.HMM.stateDistanceClusters is None):
            self.buildStructuralClusters()

        populations = self.GHMMGetPopulations()

        restarts = dict()
        registeredRestarts = 0

        for i in range(len(self.HMM.stateCentroids)):
            restarts[i] = 0

        # Because sometimes the number of restarts is lower than the number of distinct states,
        # it is important to select a conformationally diverse restart set. We do so by selecting
        # the cluster level with a number of clusters as close as possible from nRestarts. Otherwise,
        # one supercluster may contain many sparsely populated distinct states and restarts would be
        # chosen only from that single supercluster.
        clusterLevel = 0
        for i in range(len(self.HMM.stateDistanceClusters)):
            if i+1 == len(self.HMM.stateDistanceClusters):
                clusterLevel = i
                break
            nClustersAtLevel = len(self.HMM.stateDistanceClusters[i])
            if nClustersAtLevel > nRestarts:
                if i == 0:
                    break
                elif nRestarts - len(self.HMM.stateDistanceClusters[i-1]) < nClustersAtLevel - nRestarts:
                    clusterLevel = i-1
                    break
                else:
                    clusterLevel = i
                    break
            

        clusters = self.HMM.stateDistanceClusters[clusterLevel]
        while registeredRestarts < nRestarts:
            print(nRestarts)
            if nRestarts - registeredRestarts > len(clusters):
                for cluster in clusters:
                    minStateIndex = 0
                    minStatePop = math.inf

                    for i in cluster:
                        effectivePop = (restarts[i] + 1)*populations[i]
                        if effectivePop < minStatePop:
                            minStateIndex = i
                            minStatePop = effectivePop
                    
                    print("{:n}:{:f}".format(minStateIndex,minStatePop))

                    restarts[minStateIndex] += 1
                    registeredRestarts += 1
            else:
                clusterPops = []

                for i in range(len(clusters)):
                    cluster = clusters[i]
                    effectiveClusterPop = 0
                    for j in cluster:
                        effectiveClusterPop += (restarts[j] + 1)*populations[j]
                    
                    clusterPops.append(effectiveClusterPop)
                
                for pop in sorted(clusterPops):
                    if nRestarts <= registeredRestarts:
                        break
                    else:
                        for i in range(len(clusterPops)):
                            if clusterPops[i] == pop:
                                minStateIndex = 0
                                minStatePop = math.inf
                                for j in clusters[i]:
                                    effectivePop = (restarts[j] + 1)*populations[j]
                                    if effectivePop < minStatePop:
                                        minStateIndex = j
                                        minStatePop = effectivePop
                                restarts[minStateIndex] += 1
                                registeredRestarts += 1
                                print(minStateIndex)
                                break


        restartNumber = 0
        for i in restarts.keys():
            for j in range(restarts[i]):
                os.system("gmx trjconv -f {:s} -s {:s} -o {:s}_restart_{:d}.pdb -pbc mol -ur compact -b {:f} -e {:f}".format(self.solveLocation(self.input[self.HMM.stateCentroids[i][0][0]]),
                    self.solveLocation(self.tpr), self.output, restartNumber,
                    (float(self.HMM.stateCentroids[i][0][1]) - 0.5)*self.timeStep,
                    (float(self.HMM.stateCentroids[i][0][1]) + 0.5)*self.timeStep))
                
                os.system("gmx trjconv -f {:s} -s {:s} -o {:s}_restart_{:d}.gro -pbc mol -ur compact -b {:f} -e {:f} -center".format(re.sub("_prot.xtc", ".xtc",
                    self.solveLocation(self.input[self.HMM.stateCentroids[i][0][0]])), self.solveLocation(self.tpr), self.output, restartNumber,
                    (float(self.HMM.stateCentroids[i][0][1]) - 0.5)*self.timeStep,
                    (float(self.HMM.stateCentroids[i][0][1]) + 0.5)*self.timeStep))

                restartNumber += 1

    def __str__(self) -> str:
        ret = ""
        ret += "sysDir : " + str(self.sysDir) + "\n"
        ret += "input : " + str(self.input) + "\n"
        ret += "trajs : " + str(self.trajs) + "\n"
        ret += "transformedTrajs : " + str(self.transformedTrajs) + "\n"
        ret += "featureOrder : " + str(self.featureOrder) + "\n"
        ret += "topolFile : " + str(self.topolFile) + "\n"
        if self.topol is None:
            ret += "topol : None\n"
        else:
            ret += "topol :\n" + str(self.topol.to_dataframe()) + "\n"
        ret += "tpr : " + str(self.tpr) + "\n"        
        ret += "featureFile : " + str(self.featureFile) + "\n"
        ret += "textFeatures : " + str(self.textFeatures) + "\n"
        ret += "features : " + str(self.features) + "\n"
        ret += "timeStep : " + str(self.timeStep) + "ps\n"
        ret += "totalLength : " + str(round(self.totalLength,1)) + "µs\n"
        ret += "tICA : " + str(self.tICA) + "\n"
        ret += "HMM : " + str(self.HMM) + "\n"
        ret += "subMatrixScans : " + str(self.subMatrixScans) + "\n"

        return ret