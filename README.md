# TOCATA-MD
Time-Oriented Clustering and other Analysis Tools Applied to Molecular Dynamics

## Environment
Due to compatibility issues between mdtraj and msmbuilder, a specific environment must be used in orderfor the program to function :
```
conda create -n md_analysis -c conda-forge python=3 mdtraj=1.7 numpy msmbuilder scikit-learn=0.19 matplotlib
```
Examples of usage to be loaded eventually. The code is mostly self-documenting.

## Usage
Most of the required documentation is in the code itself. The general sequence to the analysis is the following :
- Initialize input parameters
- Load feature list
- Load and preprocess trajectories
- Pefrorm tICA and project trajectory on its components with highest autocorrelation
- Optimize GHMM with modified BIC and second timescale maximization
- Perform tICA sequence analysis, if needed
- Output results and simulation restart conformations