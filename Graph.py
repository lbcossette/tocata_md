from copy import copy
from math import inf

class Vertex:
    def __init__(self,label:int):
        self.label = label
        self.edges = []
        self.content = None

    def addEdge(self,newEdge): # newEdge must be an edge
        if newEdge.startVertex == self or newEdge.endVertex == self:
            unique = True
            for edge in self.edges:
                if edge.label == newEdge.label:
                    unique = False
                    break

            if unique:
                self.edges.append(newEdge)

    def removeEdge(self,toRemove): # toRemove must be an edge
        for i in range(len(self.edges)):
            if self.edges[i] == toRemove:
                self.edges.pop(i)
                if toRemove.startVertex != toRemove.endVertex:
                    if self == toRemove.endVertex:
                        toRemove.startVertex.removeEdge(toRemove)
                    else:
                        toRemove.endVertex.removeEdge(toRemove)
                break

    def __int__(self):
        return self.label

    def __eq__(self, other):
        # Equality is done through labels.
        # This means : do not use the same label for different Vertexs
        return self.label == other.label

class Edge:
    def __init__(self,label:int,startVertex:Vertex,endVertex:Vertex,oriented:bool=False,weight:float=1.0):
        self.label = label
        self.weight = weight
        self.startVertex = startVertex
        self.endVertex = endVertex
        self.oriented = oriented

        self.startVertex.addEdge(self)
        self.endVertex.addEdge(self)

    def invert(self):
        tmp = self.startVertex
        self.startVertex = self.endVertex
        self.endVertex = tmp

    def connects(self,startVertex:Vertex,endVertex:Vertex):
        if self.oriented:
            return self.startVertex == startVertex and self.endVertex == endVertex
        else:
            return (self.startVertex == startVertex and self.endVertex == endVertex) or (self.startVertex == endVertex and self.endVertex == startVertex)
    
    def getNeighbour(self, otherLabel:int):
        if self.startVertex.label == otherLabel:
            return self.endVertex
        elif self.endVertex.label == otherLabel:
            return self.startVertex

    def __int__(self):
        return self.label

    def __eq__(self,other:int):
        # Equality is done through labels.
        # This means : do not use the same label for different vertices
        return self.label == other.label

class Graph:
    def __init__(self,vertices:list,edges:list):
        self.vertices = vertices
        self.edges = edges

    def getVertexByLabel(self, label:int) -> Vertex:
        for vertex in self.vertices:
            if vertex.label == label:
                return vertex

        return None

    def addVertex(self,newVertex:Vertex):
        unique = True

        for vertex in self.vertices:
            if vertex == newVertex:
                unique = False
                break
        
        if unique:
            self.vertices.append(newVertex)

            for edge in newVertex.edges:
                self.addEdge(edge)

    def removeVertexByLabel(self, label:int):
        vertexRef = None

        for i in range(len(self.vertices)):
            if self.vertices[i].label == label:
                vertexRef = self.vertices.pop(i)
                break

        for edge in vertexRef.edges:
            self.removeEdge(edge)

    def addEdge(self,newEdge:Edge):
        unique = True

        for edge in self.edges:
            if edge.label == newEdge.label:
                unique = False
                break

        if unique:
            self.edges.append(newEdge)
            self.addVertex(newEdge.startVertex)
            self.addVertex(newEdge.endVertex)
    
    def removeEdge(self, toRemove:Edge):
        edgeRef = None

        for i in range(len(self.edges)):
            if self.edges[i] == toRemove:
                edgeRef = self.edges.pop(i)
                break

        edgeRef.startVertex.removeEdge(edgeRef)
    
    def removeEdgeByLabel(self, label:int):
        edgeRef = None

        for i in range(len(self.edges)):
            if self.edges[i].label == label:
                edgeRef = self.edges.pop(i)
                break

        edgeRef.startVertex.removeEdge(edgeRef)

    def addNewVertex(self,label:int):
        if not self.hasVertex(label):
            self.vertices.append(Vertex(label))

    def addNewEdge(self,label:int,startVertexLabel:int,endVertexLabel:int,oriented=False,weight=1.0):
        if not self.hasEdge(label):
            startVertex = self.getVertexByLabel(startVertexLabel)
            endVertex = self.getVertexByLabel(endVertexLabel)

            if (not (startVertex is None) and not (endVertex is None)):
                self.addEdge(Edge(label,startVertex,endVertex,oriented,weight))

    def subGraphs(self):
        tempVertices = copy(self.vertices)

        subGraphs = []

        while(len(tempVertices) > 0):
            subGraph = Graph([],[])

            # Because addVertex and addEdge are calling each other recursvely,
            # this insertion will trigger the insertion of an entire subgraph.
            subGraph.addVertex(tempVertices.pop())

            subGraphs.append(subGraph)

            i=0
            while i < len(tempVertices):
                found = False
                for Vertex in subGraph.vertices:
                    if tempVertices[i] == Vertex:
                        tempVertices.pop(i)
                        found = True
                        break

                if not found:
                    i += 1
        
        return subGraphs

    def hasVertex(self,label:int):
        for vertex in self.vertices:
            if vertex.label == label:
                return True
        
        return False

    def hasEdge(self,label:int):
        for edge in self.edges:
            if edge.label == label:
                return True
        
        return False

    def dijkstra(self,startLabel,endLabel):
        if not self.hasVertex(startLabel) or not self.hasVertex(endLabel):
            raise Exception("Trying to find the shortest path between two vertices that are not both in the graph!")
        
        # Verify if both vertices are in the same subgraph
        subGraphs = self.subGraphs()
        relevantSubGraph = None

        for subGraph in subGraphs:
            if (subGraph.hasVertex(startLabel) and not subGraph.hasVertex(endLabel)) or (subGraph.hasVertex(endLabel) and not subGraph.hasVertex(startLabel)):
                return ([],inf)
            elif (subGraph.hasVertex(startLabel) and subGraph.hasVertex(endLabel)):
                relevantSubGraph = subGraph

        # Create vertex set
        vertexSet = copy(relevantSubGraph.vertices)

        # Create distance and previous dicts
        distances = dict()
        previous = dict()
        for vertex in vertexSet:
            distances[vertex.label] = inf
            previous[vertex.label] = None
        distances[startLabel] = 0


        while len(vertexSet) != 0:
            minDist = inf
            minI = len(vertexSet) - 1

            for i in range(len(vertexSet)):
                dist = distances[vertexSet[i].label]
                if dist < minDist:
                    minDist = dist
                    minI = i

            vertex = vertexSet.pop(minI)

            if vertex.label == endLabel:
                break

            dist = distances[vertex.label]

            for edge in vertex.edges:
                neighbour = edge.getNeighbour(vertex.label)

                alt = distances[vertex.label] + edge.weight

                if distances[neighbour.label] > alt:
                    distances[neighbour.label] = alt
                    previous[neighbour.label] = vertex.label
        
        path = [endLabel]

        currLabel = endLabel
        while not previous[currLabel] is None:
            path.append(previous[currLabel])
            currLabel = previous[currLabel]

        return (list(reversed(path)), distances[endLabel])


    def aStar(self,startLabel:int,endLabel:int):
        pass

    def __eq__(self,other):
        #print("Entered __eq__.")
        for selfVertex in self.vertices:
            absent = True
            for otherVertex in other.vertices:
                if selfVertex == otherVertex:
                    absent = False
                    break
            
            if absent:
                return False
        
        for selfEdge in self.edges:
            absent = True
            for otherEdge in other.edges:
                if selfEdge == otherEdge:
                    absent = False
                    break
            
            if absent:
                return False
        
        return True
        

def testGraph():
    vert1 = Vertex(1)
    vert2 = Vertex(2)
    vert3 = Vertex(3)
    vert4 = Vertex(4)

    vert5 = Vertex(5)
    vert6 = Vertex(6)
    vert7 = Vertex(7)

    Edge(1,vert1,vert2)
    Edge(2,vert1,vert3)
    Edge(3,vert2,vert4)
    Edge(4,vert3,vert4)

    Edge(5,vert5,vert6)
    Edge(6,vert5,vert7)
    Edge(7,vert6,vert7)

    graph1 = Graph([],[])
    graph2 = Graph([],[])
    graph3 = Graph([],[])

    graph1.addVertex(vert1)
    graph1.addVertex(vert5)

    graph2.addVertex(vert1)

    graph3.addVertex(vert5)

    if len(graph1.vertices) != 7:
        raise Exception("Graph recursive Vertex insertion failed.")

    if len(graph1.vertices) != 7:
        raise Exception("Graph recursive Edge insertion failed.")

    subGraphs = graph1.subGraphs()

    if not (subGraphs[0] == graph2 or subGraphs[0] == graph3) or not (subGraphs[1] == graph2 or subGraphs[1] == graph3):
        raise Exception("SubGraphs failed.")
    
    graph1.addEdge(Edge(8,vert4,vert5))

    subGraphs = graph1.subGraphs()

    if len(subGraphs) != 1 or len(subGraphs[0].vertices) != 7 or len(subGraphs[0].edges) != 8:
        raise Exception("Subgraphs failed with connex graph.")

    if(graph1.dijkstra(1,7)[1] != 4.0):
        raise Exception("Dijkstra failed for two connected nodes")
    
    graph1.removeEdgeByLabel(8)

    if(graph1.dijkstra(1,7)[1] != inf):
        raise Exception("Dijkstra failed for two non-connected nodes")

testGraph()