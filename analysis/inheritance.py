from msmbuilder.hmm import GaussianHMM as GHMM
import numpy as np
import math
from copy import deepcopy

def bhattacharrya(mean1, var1, mean2, var2):
#
	det1 = 1.0
	det2 = 1.0
	det12 = 1.0
	
	for i in range(len(var1)):
	#
		det1 *= var1[i]
		
		det2 *= var2[i]
		
		det12 *= 0.5*(var1[i] + var2[i])
	#
	
	
	term1 = math.log(det12/math.sqrt(det1*det2))
	
	#print("Term 1 : {:f}".format(term1))
	
	term2 = 0.0
	
	for i in range(len(var1)):
	#
		term2 += 0.125*(mean1[i] - mean2[i])**2*2.0/(var1[i] + var2[i])
	#
	
	#print("Term 2 : {:f}".format(term2))
	
	return term2+term1
#

def bhattacharrya_vector(mean1, var1, mean2, var2):
#
	component_terms = []
	
	for i in range(len(var1)):
	#
		term = 0.0
		
		term += 0.125*(mean1[i] - mean2[i])**2*2.0/(var1[i] + var2[i]) + math.log((var2[i]+var1[i])/math.sqrt(var2[i]*var1[i])*0.5)
		
		component_terms.append(term)
	#
	
	return component_terms
#


########################################################################
#																	   #
#	 		  					Node class							   #
#																	   #
########################################################################


class Node:
#
	def __init__(self, parent, location):
	#
		self.data = [] # .. # The leaves
		self.next = [] # .. # The child nodes
		self.parent = parent # .. # Location of the parent node (single inheritance)
		self.location = location # .. # Location of the node in the tree
	#
	
	def add_next(self, next):
	#
		#print("Adding next node...")
		
		location = deepcopy(self.location)
		
		location.append(len(self.next))
		
		#print("Parent location :")
		#print(self.location)
		#print("Node location :")
		#print(location)
		
		if next is None:
		#
			new_node = Node(self.location, location)  # .. # Creates a new node to append
			
			self.next.append(new_node)
		#
		else:
		#
			next.parent = self.location
			next.location = location
			
			self.next.append(next) # .. # Appends an existing node
		#
	#
	
	def add_data(self, data): # .. # Appends a single to data to existing data
	#
		self.data.append(data)
	#
	
	def extend_data(self, data): # .. # Appends a list of data to existing data
	#
		self.data.extend(data)
	#
	
	def print_node(self, dest):
	#
		dest.write("(")
		
		if len(self.data) > 0:
		#
			dest.write("State {:s}".format(" ".join(map(str, self.data))))
			
			if len(self.next) > 0:
			#
				dest.write(",")
			#
		#
		
		if len(self.next) > 0:
		#
			self.next[0].print_node(dest)
			
			if len(self.next) > 1:
			#
				for i in range(1, len(self.next)):
				#
					dest.write(",")
					
					self.next[i].print_node(dest)
				#
			#
		#
		
		dest.write(")")
	#
	
	def move(self): # Move a node somewhere else in the graph
	#
		location = deepcopy(self.location)
		
		location.append(0)
		
		for i in range(len(self.next)):
		#
			location[-1] = i
			
			self.next[i].location = deepcopy(location)
			
			self.next[i].parent = self.location
			
			self.next[i].move()
		#
	#
	
	def search_data(self, data, found):
	#
		for i in self.data:
		#
			if i == data:
			#
				found.append(self.location)
			#
		#
		
		for node in self.next:
		#
			node.search_data(data, found)
		#
	#
	
	def remove(self): # .. # Destructor of node class
	#
		while self.next: # .. # Resursive deletion of all child nodes
		#
			self.next[-1].remove()
			
			del self.next[-1]
		#
		
		self.data = []
		self.parent = []
		self.location = []
		
		del self.next
		del self.data
		del self.parent
		del self.location
	#
#


########################################################################
#																	   #
#	 		  					Tree class							   #
#																	   #
########################################################################


class Tree:
#
	def __init__(self):
	#
		self.head = Node(None, [])
	#
	
	def get_node(self, location):
	#
		if not location:
		#
			return self.head
		#
		else:
		#
			current = self.head.next[location[0]]
			
			for i in range(1,len(location)):
			#
				current = current.next[location[i]]
			#
			
			return current
		#
	#
	
	def deepen(self, location, levels):
	#
		current = self.get_node(location)
		
		for i in range(levels):
		#
			current.add_next(None)
			
			current = current.next[-1]
		#
	#
	
	def broaden(self, location, n_new):
	#
		current = self.get_node(location)
		
		for i in range(n_new):
		#
			current.add_next(None)
		#
	#
	
	def add_data(self, location, data):
	#
		current = self.get_node(location)
		
		current.add_data(data)
	#
	
	def move_node(self, location1, location2):
	#
		cargo = self.get_node(location1)
		
		dest = self.get_node(location2)
		
		parent = self.get_node(cargo.parent)
		
		dest.add_next(cargo)
		
		cargo.move()
		
		del parent.next[location1[len(location1) - 1]]
		
		parent.move()
	#
	
	def move_data(self, location1, pos, location2):
	#
		giver = self.get_node(location1)
		
		taker = self.get_node(location2)
		
		data = giver.data[pos]
		
		del giver.data[pos]
		
		taker.add_data(data)
	#
	
	def move_all_data(self, location1, location2):
	#
		giver = self.get_node(location1)
		
		taker = self.get_node(location2)
		
		data = deepcopy(giver.data)
		
		taker.extend_data(data)
		
		del data
		
		giver.data = []
	#
	
	def delete_node(self, location):
	#
		current = self.get_node(location)
		
		parent = self.get_node(current.parent)
		
		pos = deepcopy(current.location[-1])
		
		current.remove()
		
		del parent.next[pos]
		
		del pos
		
		loc = deepcopy(parent.location)
		
		loc.append(0)
		
		for i in range(len(parent.next)):
		#
			loc[-1] = i
			
			parent.next[i].location = deepcopy(loc)
			
			parent.next[i].move()
		#
	#
	
	def delete_data(self, location, pos):
	#
		current = self.get_node(location)
		
		del current.data[pos]
	#
	
	def wipe_data(self, location):
	#
		current = self.get_node(location)
		
		current.data = []
	#
	
	def print_tree(self, dest):
	#
		self.head.print_node(dest)
		
		dest.write(";")
	#
	
	def search_data(self, data):
	#
		if not (isinstance(data, int), isinstance(data, float), isinstance(data, str)):
		#
			raise Exception("Cannot search for given type of data")
		#
		else:
		#
			found = []
			
			self.head.search_data(data, found)
			
			return found
		#
	#
#


########################################################################
#																	   #
#	 		  				Inheritance class						   #
#																	   #
########################################################################


class inheritance:
#
	def __init__(self, hmm_dict, freq, dest):
	#
		self.means = hmm_dict['means']
		self.sigmas = hmm_dict['vars']
		self.pops = hmm_dict['popls']
		self.trans = hmm_dict['transmat']
		self.superstates = Tree()
		self.node = dict()
		self.lumps = []
		self.trans_prob = freq
		self.dest = dest
		
		self.superstates.deepen(None, len(self.means[0]) - 1)
		
		loc = [0 for i in range(len(self.means[0]) - 1)]
		
		self.superstates.broaden(loc, len(self.means))
		
		loc.append(-1)
				
		for i in range(len(self.means)):
		#
			loc[-1] = i
			
			self.superstates.add_data(loc, i)
			
			self.node[i] = self.superstates.get_node(loc)
		#
		
		#self.superstates.print_tree()
	#
	
	def same_lump(self, state1, state2):
	#
		if len(self.node[state1].location) == len(self.node[state2].location):
		#
			for i in range(len(self.node[state1].location)):
			#
				if self.node[state1].location[i] != self.node[state2].location[i]:
				#
					return False
				#
			#
		#
		else:
		#
			return False
		#
		
		return True
	#
	
	def lump(self):
	#
		for i in range(len(self.means)-1):
		#
			for j in range(i+1, len(self.means)):
			#
				#print("Test {:d} {:d}\n".format(i,j))
				
				if not self.same_lump(i,j):
				#
					bhat = bhattacharrya(self.means[i], self.sigmas[i], self.means[j], self.sigmas[j])
					
					#print("Bhattacharrya distance : {:f}\n".format(bhat))
					
					# Lumping with a 2.5% average classification error (Choi E, Lee C. Pattern Recognition. 2003. Vol 36.p1703-1709)
					
					if bhat < 2.81599 and (self.trans[i][j] >= self.trans_prob or self.trans[j][i] >= self.trans_prob):
					#
						#print("Lumping states {:d} and {:d}...".format(i,j))
						
						self.superstates.move_all_data(self.node[j].location, self.node[i].location)
						
						to_del = deepcopy(self.node[j].location)
						
						for k in self.node[i].data:
						#
							self.node[k] = self.node[i]
						#
						
						self.superstates.delete_node(to_del)
						
						del to_del
					#
				#
			#
		#
		
		print("Lumping over :\n")
		
		#self.superstates.print_tree()
		
		print("\n")
		
		#print(self.node)
		for i in self.node:
		#
			print(i)
			print(self.node[i].data)
		#
		
		print("\n")
	#
	
	def match_lumps(self, node1, node2, means, sigmas):
	#
		match = False
		
		for i in node1.data:
		#
			for j in node2.data:
			#
				bhat = bhattacharrya(means[i], sigmas[i], means[j], sigmas[j])
				
				#print("Bhattacharrya distance : {:f}\n".format(bhat))
				
				if bhat < 2.81599 and (self.trans[i][j] >= self.trans_prob or self.trans[j][i] >= self.trans_prob):
				#
					match = True
				#
			#
		#
		
		if not match:
		#
			for node_1 in node1.next:
			#
				for node_2 in node2.next:
				#
					match = self.match_lumps(node_1, node_2, means, sigmas)
				#
			#
		#
				
		if not match:
		#
			for node in node2.next:
			#
				match = self.match_lumps(node1, node, means, sigmas)
			#
		#
		
		if not match:
		#
			for node in node1.next:
			#
				match = self.match_lumps(node, node2, means, sigmas)
			#
		#
		
		return match
	#
	
	def retrolump(self):
	#
		means = self.means
		sigmas = self.sigmas
		
		parent_loc = deepcopy(self.node[0].location)
		
		while len(means[0]) > 1:
		#
			for mean in means:
			#
				del mean[-1]
			#
			
			for sigma in sigmas:
			#
				del sigma[-1]
			#
			
			del parent_loc[-1]
			
			parent_node = self.superstates.get_node(parent_loc)
			
			grandparent_node = self.superstates.get_node(parent_node.parent)
			
			while parent_node.next:
			#
				self.superstates.broaden(grandparent_node.location, 1)
				
				new_parent = grandparent_node.next[-1]
				
				self.superstates.move_node(parent_node.next[0].location, new_parent.location)
				
				i = -1
				
				while i < len(new_parent.next) - 1:
				#
					i += 1
					
					j = -1
					
					while j < len(parent_node.next) - 1:
					#
						j += 1
						
						while self.match_lumps(new_parent.next[i], parent_node.next[j], means, sigmas):
						#
							self.superstates.move_node(parent_node.next[j].location, new_parent.location)
							
							if j >= len(parent_node.next):
							#
								break
							#
						#
					#
				#
			#
			
			self.superstates.delete_node(parent_loc)
		#
		
		self.superstates.print_tree(self.dest)
	#
#
