from matplotlib import pyplot as plt
import numpy as np
from scipy.stats import gaussian_kde
from matplotlib.patches import Ellipse
import math
from MDSystem import MDSystem

def plotSimple(x: np.array, xTickLabels:list, y: np.array, datasetLabel:str, xTitle: str, yTitle: str,titleText: str) -> None:
    plt.plot(x,y)
    plt.title(titleText)
    plt.show()
    plt.close()

def plotMultiple(x: np.array, xTickLabels:list, ys:list, datasetLabels:list, xTitle: str, yTitle: str, titleText:str) -> None:
    for i in range(len(ys)):
        plt.plot(x, ys[i], label = datasetLabels[i])
        
    plt.xticks(x,xTickLabels, rotation=90)

    for i in x:
        plt.axvline(i, alpha=0.3, color="0.75")

    plt.xlabel(xTitle)
    plt.ylabel(yTitle)
    plt.title(titleText)
    plt.legend(loc="lower right")
    plt.show()
    plt.close()

def plotPolarMultiple(x:np.array, ys:list, datasetLabels:list, titleText:str, savePath:str):
    for i in range(len(ys)):
        plt.polar(x,ys[i],label=datasetLabels[i],linewidth=0.4)
    for i in range(0,72):
        plt.axvline(i*math.pi/36, alpha=0.3, color="0.75")
    plt.legend(loc="best", ncol=2,fontsize=8)
    plt.title(titleText)
    plt.savefig(savePath,format="png",dpi=300)
    plt.close()

def plotHeatMap(mat, minVal, maxVal, titleText:str, save=None) -> None:
    """
    Plot a matrix of integeror real values as a heatmap.

    mat : matrix to plot.
    minVal : lowest value to consider. All values below are considered equal to this value.
    maxVal : highest value to consider. All values above are considered equal to this value.
    """

    if maxVal <= minVal:
        raise Exception("Minimum value must be lower than maximum value.")

    tmpMat = np.zeros(mat.shape)

    for i in range(len(mat)):
        for j in range(len(mat[0])):
            # Constrain values to interval [0.0,1.0]
            tmpVal = (mat[i][j] - minVal)/(maxVal - minVal)
            if tmpVal < 0.0:
                tmpVal = 0.0
            elif tmpVal > 1.0:
                tmpVal = 1.0

            tmpMat[i][j] = tmpVal

    plt.imshow(tmpMat, cmap="winter", interpolation="nearest")
    plt.xticks(np.arange(len(tmpMat[0])))
    plt.yticks(np.arange(len(tmpMat)))
    plt.title(titleText)
    plt.show()
    plt.close()

def plotHMMAndTrajs(sys: MDSystem, titleStem: str) -> None:
    # Concatenate all trajectories
    allTrajs = sys.transformedTrajs[0]
    for i in range(1, len(sys.transformedTrajs)):
        allTrajs = np.append(allTrajs, sys.transformedTrajs[i], axis=0)
    
    means = sys.HMM.stateMeans
    varss = sys.HMM.stateVars

    # Reduce trajectory size to facilitate density calculation
    skip = int(len(allTrajs)/35000)

    if skip == 0:
        skip = 1

    toTake = np.arange(0,len(allTrajs),skip)

    allTrajs = np.take(allTrajs,toTake, axis=0)
    
    #print(allTrajs)
    #print(allTrajs.shape)

    allTrajs = np.transpose(allTrajs)

    # TODO : integrate into loop to get all plots
    for i in range(0,len(allTrajs),2):
        if len(allTrajs) - i == 1:
            def plotGaussian(bins,pop,mean,var):
                ret = np.zeros(len(bins))

                for k in range(len(bins)):
                    ret[k] = pop/math.sqrt(2*math.pi*var)*math.exp(-0.5*(bins[k]-mean)**2/var)

                return ret

            tmp = allTrajs[i]

            # Find populations
            tmpL = sys.HMM.eVals
            tmpMax = max(tmpL)
            tmpPops = np.zeros(len(means))
            tmpV = None
            for j in range(len(tmpL)):
                if tmpL[j] == tmpMax:
                    tmpV = sys.HMM.leftEVecs[j]
                    break
            totV = sum(tmpV)
            for j in range(len(tmpPops)):
                tmpPops[j] = tmpV[j]/totV

            minData = min(tmp)
            maxData = max(tmp)
            step = 0.01
            minn = minData-step
            maxx = maxData+step

            bins = np.arange(minn,maxx,step)
            counts = np.zeros(len(bins), dtype=int)

            for j in range(len(tmp)):
                counts[int((tmp[j]-minn)/step)] += 1

            plt.plot(bins,counts)

            for j in range(len(means)):
                # If the bin size is too big, there will be discrepancies between approximation and counts
                plt.plot(bins,plotGaussian(bins,tmpPops[j]*len(tmp)*step,means[j][i],varss[j][i]), label="State " + str(j))
            
            plt.title(titleStem + " {:n}".format(i))
            plt.legend(loc="upper right")
            plt.show()
        else:
            xData = allTrajs[i]
            yData = allTrajs[i+1]

            points = np.vstack([xData,yData])
            
            print("Calculating point density...")
            
            points_z = gaussian_kde(points)(points)
            
            print("Sorting points...")
            idx = points_z.argsort()
            
            x, y, z = xData[idx], yData[idx], points_z[idx]
            
            print("Rendering...")
            fig, ax = plt.subplots()
            
            ax.scatter(x, y, c=z, s=5, edgecolor='')
            
            ellipses = [Ellipse(xy = [means[j][i], means[j][i+1]], width=1.96*math.sqrt(varss[j][i]), height=1.96*math.sqrt(varss[j][i+1]), angle=0) for j in range(len(means))]

            for j in range(len(ellipses)):
                ax.add_artist(ellipses[j])
                ellipses[j].set_clip_box(ax.bbox)
                ellipses[j].set_alpha(0.3)
                ellipses[j].set_facecolor('black')
                plt.text(means[j][i]-0.025*(j//10 + 1), means[j][i+1]-0.05, j, color="w", size=14, weight="bold")

            plt.title(titleStem + "s {:n} and {:n}".format(i+1,i+2))

            plt.show()
    plt.close()