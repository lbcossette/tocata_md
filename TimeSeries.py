from msmbuilder.decomposition import tICA
from msmbuilder.hmm import GaussianHMM
import math
from abc import abstractmethod
from copy import deepcopy
import sqlite3
import numpy as np
from collections import namedtuple
from Graph import Graph, Edge, Vertex

standardizationFactors = {5 : 10, 7 : 28, 9 : 60, 11 : 110, 13 : 182, 15 : 280,
                          17 : 408, 19 : 570, 21 : 770, 23 : 1012, 25 : 1300}

intToMaj = {0:"A", 1:"B", 2:"C", 3:"D", 4:"E", 5:"F", 6:"G", 7:"H", 8:"I", 9:"J", 10:"K", 11:"L", 12:"M", 13:"N",
           14:"O", 15:"P", 16:"Q", 17:"R", 18:"S", 19:"T", 20:"U", 21:"V", 22:"W", 23:"X", 24:"Y", 25:"Z"}

TICA = namedtuple("TICA", ["autoCorrMat","covMat","eVals","eVecs","eVecsT","timescales","means","nWorthwhileComps","score"])
GHMM = namedtuple("GHMM", ["BIC","nDimensions","nClusters","timescale","propagator","timescales","eVals","rightEVecs",
"leftEVecs","stateMeans","stateVars","stateCentroids","stateDistances", "stateDistanceClusters"])

bhattacharyya25 = 1.98289 # Bhattacharyya distance for 2.5% classification error for diagonal gaussian classes

class TimeSeries:
    def __init__(self):
        self.trajs = []
        self.transformedTrajs = None
        self.features = dict() # Category and information, in order (np.array)
        self.featureOrder = [] # List of categories in order of appearance. Default usage is to store feature info when they are not in categories.
        self.maxLag = math.inf # Maximum time lag, in frames
        self.totalLength = 0 # Total trajectory length in frames
        self.tICA = None # lagtime to ndarray
        self.HMM = None # "nDimensions,nClusters" to array of gaussians, transition matrix, eigenvectors and eigenvalues
        self.subMatrixScans = [] # Tuples containing the toRemove array and the corresponding results
        #self.decorrelationScans = dict() # lagtime to array
        #self.cycloAnalyses = dict() # Cyclostationary analyses
        #self.VARs = dict() # "lagtime1,lagtime2,..." to array containing regression matrix and error matrix
        self.output = None

    def setDBPath(self, dbPath: str) -> None:
        self.dbPath = dbPath

    @abstractmethod
    def loadTrajs(self) -> None:
        # Must define self.trajs and self.featureOrder
        pass

    def dbTables(self, conn: sqlite3.Connection) -> list:
        curs = conn.cursor()

        curs.execute("SELECT name FROM sqlite_master WHERE type='table'")
        tables = curs.fetchall()
        for i in range(len(tables)):
            tables[i] = tables[i][0]

        return tables

    def tICACompute(self):
        """
        Compute the tICA and de-clutter the model by calculating the last component worthwhile to consider.
        Sources :
        http://msmbuilder.org/3.8.0/decomposition.html#tica
        Pérez-Hernández G et al. Identification of slow molecular order parameters for Markov model construction. J Chem Phys. 2013. Vol 139 (015102). p1-13
        Schwantes CR, Pande VS. Improvements in Markov State Model Construction Reveal Many Non-Native Interactions in the Folding of NTL9. J Chem Theo Comp. 2013. Vol 9 (4). p2000-2009
        """
        
        print("Computing tICA...")

        model = tICA(n_components = len(self.trajs[0][0]), lag_time = self.maxLag)
        model.fit(self.trajs)

        timeScales = model.timescales_

        lastValidTimeScaleIndex = 0

        for i in range(len(timeScales)):
            if(math.isnan(timeScales[i])):
                lastValidTimeScaleIndex = i
                break

        averageSlope = (timeScales[lastValidTimeScaleIndex-1]-timeScales[0])/lastValidTimeScaleIndex

        lastWorthwhileComponent = 1

        for i in range(2,lastValidTimeScaleIndex):
            if(self.discreteDerivative(timeScales,i,5) > averageSlope):
                lastWorthwhileComponent = i
                break

        self.tICA = TICA(None,None,None,None,None,None,None,lastWorthwhileComponent,None)

        self.tICA = self.tICA._replace(autoCorrMat = model.offset_correlation_, score = model.score_, covMat = model.covariance_,
        eVecs = model.eigenvectors_, eVecsT = model.components_, eVals = model.eigenvalues_, timescales = model.timescales_, means = model.means_)

        self.transformedTrajs = model.transform(self.trajs)

        for i in range(len(self.transformedTrajs)):
            tmp = self.transformedTrajs[i]
            tmp = tmp[:,:lastWorthwhileComponent]
            self.transformedTrajs[i] = tmp

    def tICALog(self, overwrite=False) -> None:
        print("Logging tICA...")

        conn = sqlite3.connect(self.dbPath)

        if overwrite:
            tables = self.dbTables(conn)

            if("tica_info" in tables):
                conn.execute("DROP TABLE tica_info")
            if("tica_autocorrelation_matrix" in tables):
                conn.execute("DROP TABLE tica_autocorrelation_matrix")
            if("tica_covariance_matrix" in tables):
                conn.execute("DROP TABLE tica_covariance_matrix")
            if("tica_eigenvalues" in tables):
                conn.execute("DROP TABLE tica_eigenvalues")
            if("tica_means" in tables):
                conn.execute("DROP TABLE tica_means")
            if("tica_eigenvectors" in tables):
                conn.execute("DROP TABLE tica_eigenvectors")

        conn.execute("CREATE TABLE tica_info (name TEXT, val BLOB)")
        conn.execute("CREATE TABLE tica_autocorrelation_matrix (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE tica_covariance_matrix (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE tica_eigenvalues (i UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE tica_means (i UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE tica_eigenvectors (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")

        # Save generic parameters
        conn.execute("INSERT INTO tica_info VALUES (?,?)",("lastWorthwhileComponent",self.tICA.nWorthwhileComps))
        conn.execute("INSERT INTO tica_info VALUES (?,?)",("score",self.tICA.score))
        conn.execute("INSERT INTO tica_info VALUES (?,?)",("lagTime",self.maxLag))

        tmpA = self.tICA.autoCorrMat
        tmpB = self.tICA.covMat
        tmpV = self.tICA.eVecs
        """
        self.tICA["model"].components_ is actually the transpose of self.tICA["model"].eigenvectors_
        I guess this is for convenience, at least I hope. Here is some code to prove it :

        testcomp = self.tICA["model"].components_[0]
        testevec = self.tICA["model"].eigenvectors_[:,0]

        rescomp = np.matmul(np.matmul(testcomp,model.offset_correlation_),np.transpose(testcomp))
        resevec = np.matmul(np.matmul(np.transpose(testevec),model.offset_correlation_),testevec)

        # These two values are equal up to the fifth decimal
        print(rescomp)
        print(resevec)
        print(rescomp == resevec or math.abs(rescomp - resevec) <= 0.00001)
        """
        tmpL = self.tICA.eVals
        tmpM = self.tICA.means

        # Save all essential parameters
        for i in range(len(tmpA)):
            for j in range(len(tmpA)):
                conn.execute("INSERT INTO tica_autocorrelation_matrix VALUES (?,?,?)",(i,j,tmpA[i][j]))
                conn.execute("INSERT INTO tica_covariance_matrix VALUES (?,?,?)",(i,j,tmpB[i][j]))
                conn.execute("INSERT INTO tica_eigenvectors VALUES (?,?,?)",(i,j,tmpV[i][j]))
            
            conn.execute("INSERT INTO tica_eigenvalues VALUES (?,?)",(i,tmpL[i]))
            conn.execute("INSERT INTO tica_means VALUES (?,?)",(i,tmpM[i]))

        conn.commit()
        conn.close()

    def tICALoadResultsFromSQL(self) -> None:
        conn = sqlite3.connect(self.dbPath)
        curs = conn.cursor()

        # Load generic parameters
        curs.execute("SELECT * FROM tica_info")
        table = curs.fetchall()

        tmpScore = 0.0
        tmpLastWorthwhileComponent = 0
        for ele in table:
            if ele[0] == "score":
                tmpScore = ele[1]
            elif ele[0] == "lagTime":
                self.maxLag = ele[1]
            else:
                tmpLastWorthwhileComponent = ele[1]

        # Get the rest of the required info to create full tICA object
        curs.execute("SELECT * FROM tica_autocorrelation_matrix")
        table = curs.fetchall()

        nComponents = int(math.sqrt(len(table)))

        self.tICA = TICA(None,None,None,None,None,None,None,tmpLastWorthwhileComponent,tmpScore)

        # Fill offset_correlation_
        tmpOffsetCorrelation = np.zeros((nComponents,nComponents))
        for ele in table:
            tmpOffsetCorrelation[ele[0]][ele[1]] = ele[2]

        # Fill covariance_
        curs.execute("SELECT * FROM tica_covariance_matrix")
        table = curs.fetchall()

        tmpCovariance = np.zeros((nComponents,nComponents))
        for ele in table:
            tmpCovariance[ele[0]][ele[1]] = ele[2]

        # Fill eigenvectors_ and components_
        curs.execute("SELECT * FROM tica_eigenvectors")
        table = curs.fetchall()

        tmpEigenvectors = np.zeros((nComponents,nComponents),"F")
        tmpComponents = np.zeros((nComponents,nComponents))
        for ele in table:
            tmpEigenvectors[ele[0]][ele[1]] = ele[2]
            tmpComponents[ele[1]][ele[0]] = ele[2]

        # Fill eigenvalues_ and timescales_
        curs.execute("SELECT * FROM tica_eigenvalues")
        table = curs.fetchall()

        tmpEigenvalues = np.zeros(nComponents)
        for ele in table:
            tmpEigenvalues[ele[0]] = ele[1]

        # Fill means_
        curs.execute("SELECT * FROM tica_means")
        table = curs.fetchall()

        tmpMeans = np.zeros(nComponents)
        for ele in table:
            tmpMeans[ele[0]] = ele[1]

        self.tICA = self.tICA._replace(autoCorrMat = tmpOffsetCorrelation, covMat = tmpCovariance, eVecs = tmpEigenvectors, eVecsT = tmpComponents, eVals = tmpEigenvalues,
        timescales = -1. * self.maxLag / np.log(tmpEigenvalues), means = tmpMeans)

        conn.close()

        if self.trajs:
            self.transformedTrajs = []
            for traj in self.trajs:
                # Transformation is essentially dot-product of a frame with each eigenvector
                self.transformedTrajs.append(np.real(np.matmul(traj - self.tICA.means,self.tICA.eVecs)))

            for i in range(len(self.transformedTrajs)):
                tmp = self.transformedTrajs[i]
                tmp = tmp[:,:self.tICA.nWorthwhileComps]
                self.transformedTrajs[i] = tmp

    def genSymSubMatrixAnalysis(self, toRemove: list, A: np.array, B: np.array) -> tuple:
        """
        Perform a principal submatrix analysis on equation AV=BVΛ by removing indices in toRemove.
        Matrices A and B must be symmetric, and B positive definite
        Quick reference on the subject :
        http://fourier.eng.hmc.edu/e161/lectures/algebra/node7.html
        Input :
            toRemove : list of indices to remove
            mat1	 : np.ndarray 
        """
        toRemove = sorted(toRemove)
        toKeep = list(range(len(A)))

        while(toRemove):
            toKeep.pop(toRemove.pop())

        # Create principal submatrices
        if len(A) != len(B) or len(A[0]) != len(B[0]):
            raise Exception("Cannot analyze A of shape " + A.shape + " when B is of shape " + B.shape + "!")
        else:
            B = np.take(np.take(B,toKeep,axis=0),toKeep,axis=1)

        A = np.take(np.take(A,toKeep,axis=0),toKeep,axis=1)

        #print(A.shape)
        #print(B.shape)

        bEvals, bEvecs = np.linalg.eigh(B)

        # Calculate reduced eigenvectors of B
        bEvecs = bEvecs * (1. / np.sqrt(bEvals))

        #print(bEvecs)

        # Calculate A'
        aPrime = np.matmul(np.matmul(np.transpose(bEvecs),A),bEvecs)

        aPrimeEvals, aPrimeEvecs = np.linalg.eigh(aPrime)

        #print(aPrimeEvals)
        #print(aPrimeEvecs)

        ret = (toRemove, aPrimeEvals, np.matmul(bEvecs,aPrimeEvecs))

        self.subMatrixScans.append(ret)

        return (toKeep,ret[1],ret[2])

    def tICAFeaturePerturbationAnalysis(self,minDim: int,maxDim: int) -> np.array:
        """
        DEPRECATED. Calculates the eigenvalue perturbation for each component.
        Assumes that a given component is decorrelated from itself and the system
        by simulating the effect of added noise. This perturbation has computable
        effects when it is small.

        ret[i][j] is the result of the modified matrix equation for dli :
        (A + dA)(vi+dvi) = (li + dli)B(vi+dvi) (dB = 0)
        with dA containing a small multiple (m->0) of each term with row or column index j
        """

        tmpEVecsT = self.tICA.eVecsT
        tmpC = self.tICA.autoCorrMat
        ret = np.zeros((maxDim-minDim,len(tmpC)))

        for i in range(minDim,maxDim):
            for j in range(len(tmpC)):
                ret[i][j] = tmpEVecsT[i][j]*(2.0*np.vdot(tmpEVecsT[i],tmpC[j]) - tmpC[j][j]*tmpEVecsT[i][j])

        return ret

    @staticmethod
    def tICACompare(rightEvecs1:np.array, toRemove1:list, rightEvecs2:np.array, toRemove2:list):
        """
        Compare eigenvectors from which components in their respective toRemove arrays have been removed.
        Returns a matrix containing all scalar products of eigenvectors from system 1 with those from system 2.
        Useful only when differences between two systems are minor (e.g. point mutation, or same protein under different conditions)
        """

        toRemove1 = sorted(toRemove1)
        toKeep1 = list(range(len(rightEvecs1)))
        while(toRemove1):
            toKeep1.pop(toRemove1.pop())
        rightEvecs1 = np.transpose(np.take(rightEvecs1,toKeep1,axis=0))
        for i in range(len(rightEvecs1)):
            rightEvecs1[i] = rightEvecs1[i]/np.linalg.norm(rightEvecs1[i])

        toRemove2 = sorted(toRemove2)
        toKeep2 = list(range(len(rightEvecs2)))
        while(toRemove2):
            toKeep2.pop(toRemove2.pop())
        rightEvecs2 = np.transpose(np.take(rightEvecs2,toKeep2,axis=0))
        for i in range(len(rightEvecs2)):
            rightEvecs2[i] = rightEvecs2[i]/np.linalg.norm(rightEvecs2[i])

        return np.matmul(rightEvecs1, np.transpose(rightEvecs2))

    def GMHMMOptimize(self) -> None:
        """
        Calculate the optimal GHMM for the system.
        Uses BIC minimization for the number of states and second eigenvalue maximization for number of dimensions
        Sources :
        http://msmbuilder.org/3.5.0/_hmm/msmbuilder.hmm.GaussianHMM.html
        McGibbon, Robert T. et al. Understanding Protein Dynamics with L1-Regularized Reversible Hidden Markov Models. Proc 31st Intl Conf on Machine Learning (ICML). 2014.
        Noé F, Nüske F. A Variational Approach to Modeling Slow Processes in Stochastic Dynamical Systems. Multiscale Model Simul. 2013. Vol 11 (2). p635-655
        """

        optimalModels = []
        timeScaleStrikes = 0
        optimalTrajs = []

        for i in range(1,self.tICA.nWorthwhileComps + 1):
            print("{:n} dimension(s)...".format(i))
            ithSlicedTrajs = deepcopy(self.transformedTrajs)

            for j in range(len(ithSlicedTrajs)):
                tmp = ithSlicedTrajs[j]
                tmp = tmp[:,:i]
                ithSlicedTrajs[j] = tmp

            initialBIC = math.nan
            bicStrikes = 0

            ithModelSeries = []
            nClusters = 0

            while True:
                nClusters += 1

                print("\n{:n} states...".format(nClusters))

                currentModel = GaussianHMM(n_states=nClusters, reversible_type='transpose', thresh=1e-4, init_algo='GMM', timing=True)
                currentModel.fit(ithSlicedTrajs)
                currentBIC = (2.0 * float(i)) * float(nClusters) * math.log(self.totalLength) - 2.0 * currentModel.fit_logprob_[-1]
                evals = np.sort(np.linalg.eigvals(currentModel.transmat_))[::-1]

                if nClusters == 1:
                    ithModelSeries.append({"model" : currentModel, "BIC" : currentBIC, "nDimensions" : i, "nClusters" : nClusters, "timescale" : 0.0})
                else:
                    # Timescale is stored as the eigenvalue because of numerical stability reasons
                    ithModelSeries.append({"model" : currentModel, "BIC" : currentBIC,
                     "nDimensions" : i, "nClusters" : nClusters, "timescale" : evals[1]})

                print(ithModelSeries[-1])

                if nClusters >= 3:
                    if ithModelSeries[0]["BIC"] - ithModelSeries[2]["BIC"] < 1.05*(ithModelSeries[0]["BIC"] - ithModelSeries[1]["BIC"]):
                        bicStrikes += 1
                        print("Strike {:n} on number of clusters".format(bicStrikes))
                        ithModelSeries.pop()
                    else:
                        bicStrikes = 0
                        ithModelSeries.pop(1)
                elif math.isnan(initialBIC):
                    initialBIC = currentBIC

                if bicStrikes == 3:
                    print("Three strikes on number of clusters. Model series {:n} is over.".format(i))
                    optimalModels.append(ithModelSeries[1])
                    optimalTrajs.append(ithSlicedTrajs)
                    break
            
            if i >= 2:
                # Equivalent to (new timescale) < 1.05 * (old timescale)
                if math.pow(optimalModels[1]["timescale"],1.05) < optimalModels[0]["timescale"]:
                    timeScaleStrikes += 1
                    print("Strike {:n} on dimensions".format(timeScaleStrikes))
                    optimalModels.pop()
                    optimalTrajs.pop()
                elif optimalModels[1]["timescale"] >= 1.0:
                    print("Second timescale is infinite. Continuing model selection in pointless.")
                    timeScaleStrikes = 3
                    optimalModels.pop(0)
                    optimalTrajs.pop(0)
                else:
                    timeScaleStrikes = 0
                    optimalModels.pop(0)
                    optimalTrajs.pop(0)
            
            if timeScaleStrikes == 3 or i == self.tICA.nWorthwhileComps:
                self.HMM = optimalModels[0]
                self.transformedTrajs = optimalTrajs[0]
                print("Optimal model chosen with {:n} dimensions, {:n} clusters, with a characteristic timescale of {:f}.".format(self.HMM["nDimensions"],
                self.HMM["nClusters"], self.HMM["timescale"]))
                break
        
        print(self.HMM)
        eVals, rightEVecs = np.linalg.eig(self.HMM["model"].transmat_)
        tmpGHMM = GHMM(self.HMM["BIC"],self.HMM["nDimensions"],self.HMM["nClusters"],self.HMM["timescale"],self.HMM["model"].transmat_,
        self.HMM["model"].timescales_,eVals,rightEVecs,np.linalg.inv(rightEVecs),None,None,self.HMM["model"].draw_centroids(self.transformedTrajs)[0],None, None)

        if self.HMM["nDimensions"] == 1:
            tmpGHMM = tmpGHMM._replace(stateMeans = self.HMM["model"].means_.reshape((self.HMM["nClusters"],1)),
            stateVars = self.HMM["model"].vars_.reshape((self.HMM["nClusters"],1)))
        else:
            tmpGHMM = tmpGHMM._replace(stateMeans = self.HMM["model"].means_, stateVars = self.HMM["model"].vars_)

        self.HMM = tmpGHMM

        print(self.HMM)

    def GMHMMLog(self, overwrite=False) -> None:
        conn = sqlite3.connect(self.dbPath)

        if overwrite:
            tables = self.dbTables(conn)

            if "hmm_info" in tables:
                conn.execute("DROP TABLE hmm_info")
            if "hmm_propagator" in tables:
                conn.execute("DROP TABLE hmm_propagator")
            if "hmm_timescales" in tables:
                conn.execute("DROP TABLE hmm_timescales")
            if "hmm_left_eigenvectors" in tables:
                conn.execute("DROP TABLE hmm_left_eigenvectors")
            if "hmm_right_eigenvectors" in tables:
                conn.execute("DROP TABLE hmm_right_eigenvectors")
            if "hmm_eigenvalues" in tables:
                conn.execute("DROP TABLE hmm_eigenvalues")
            if "hmm_state_means" in tables:
                conn.execute("DROP TABLE hmm_state_means")
            if "hmm_state_vars" in tables:
                conn.execute("DROP TABLE hmm_state_vars")
            if "hmm_state_centroids" in tables:
                conn.execute("DROP TABLE hmm_state_centroids")

        conn.execute("CREATE TABLE hmm_info (name TEXT, val BLOB)")
        conn.execute("CREATE TABLE hmm_propagator (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_timescales (i UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_eigenvalues (i UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_left_eigenvectors (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_right_eigenvectors (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_state_means (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_state_vars (i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")
        conn.execute("CREATE TABLE hmm_state_centroids (i UNSIGNED INTEGER, trajN UNSIGNED INTEGER, frameN UNSIGNED INTEGER)")

        conn.execute("INSERT INTO hmm_info VALUES (?,?)",("BIC", self.HMM.BIC))
        conn.execute("INSERT INTO hmm_info VALUES (?,?)",("nDimensions", self.HMM.nDimensions))
        conn.execute("INSERT INTO hmm_info VALUES (?,?)",("nClusters", self.HMM.nClusters))
        conn.execute("INSERT INTO hmm_info VALUES (?,?)",("timescale", self.HMM.timescale))

        nClusters = self.HMM.nClusters

        tempP = self.HMM.propagator
        tempT = self.HMM.timescales
        #print(len(tempT))
        tempL = self.HMM.eVals
        #print(len(tempL))
        templV = self.HMM.leftEVecs
        temprV = self.HMM.rightEVecs
        tempMeans = self.HMM.stateMeans
        tempVars = self.HMM.stateVars
        tempCentroids = self.HMM.stateCentroids

        for i in range(nClusters):
            for j in range(nClusters):
                conn.execute("INSERT INTO hmm_propagator VALUES (?,?,?)", (i,j,float(tempP[i][j])))
                conn.execute("INSERT INTO hmm_left_eigenvectors VALUES (?,?,?)", (i,j,float(templV[i][j])))
                conn.execute("INSERT INTO hmm_right_eigenvectors VALUES (?,?,?)", (i,j,float(temprV[i][j])))
            if i != nClusters-1:
                conn.execute("INSERT INTO hmm_timescales VALUES (?,?)", (i,float(tempT[i])))
            conn.execute("INSERT INTO hmm_eigenvalues VALUES (?,?)", (i,float(tempL[i])))
            for j in range(self.HMM.nDimensions):
                conn.execute("INSERT INTO hmm_state_means VALUES (?,?,?)", (i,j,float(tempMeans[i][j])))
                conn.execute("INSERT INTO hmm_state_vars VALUES (?,?,?)", (i,j,float(tempVars[i][j])))
            conn.execute("INSERT INTO hmm_state_centroids VALUES (?,?,?)", (i, int(tempCentroids[i][0][0]), int(tempCentroids[i][0][1])))

        conn.commit()
        conn.close()

    def GHMMLoadFromSQL(self):
        conn = sqlite3.connect(self.dbPath)
        curs = conn.cursor()

        curs.execute("SELECT * FROM hmm_info")

        table = curs.fetchall()

        self.HMM = GHMM(None,None,None,None,None,None,None,None,None,None,None,None,None,None)

        for i in range(3):
            if table[i][0] == "BIC":
                self.HMM = self.HMM._replace(BIC = table[i][1])
            elif table[i][0] == "timescale":
                self.HMM = self.HMM._replace(timescale = table[i][1])
            elif table[i][0] == "nDimensions":
                self.HMM = self.HMM._replace(nDimensions = table[i][1])
            elif table[i][0] == "nClusters":
                self.HMM = self.HMM._replace(nClusters = table[i][1])


        propagator = np.zeros((self.HMM.nClusters,self.HMM.nClusters))
        leftEigenvectors = np.zeros((self.HMM.nClusters,self.HMM.nClusters))
        rightEigenvectors = np.zeros((self.HMM.nClusters,self.HMM.nClusters))
        eigenvalues = np.zeros(self.HMM.nClusters)
        stateMeans = np.zeros((self.HMM.nClusters, self.HMM.nDimensions))
        stateVars = np.zeros((self.HMM.nClusters, self.HMM.nDimensions))
        timescales = np.zeros(self.HMM.nClusters-1)
        stateCentroids = np.zeros((self.HMM.nClusters,1,2), dtype=int)

        curs.execute("SELECT * FROM hmm_propagator")
        table = curs.fetchall()

        for ele in table:
            propagator[ele[0]][ele[1]] = ele[2]

        curs.execute("SELECT * FROM hmm_left_eigenvectors")
        table = curs.fetchall()

        for ele in table:
            leftEigenvectors[ele[0]][ele[1]] = ele[2]

        curs.execute("SELECT * FROM hmm_right_eigenvectors")
        table = curs.fetchall()

        for ele in table:
            rightEigenvectors[ele[0]][ele[1]] = ele[2]

        curs.execute("SELECT * FROM hmm_eigenvalues")
        table = curs.fetchall()

        for ele in table:
            eigenvalues[ele[0]]= ele[1]

        curs.execute("SELECT * FROM hmm_timescales")
        table = curs.fetchall()

        for ele in table:
            timescales[ele[0]]= ele[1]

        curs.execute("SELECT * FROM hmm_state_means")
        table = curs.fetchall()

        for ele in table:
            stateMeans[ele[0]][ele[1]] = ele[2]

        curs.execute("SELECT * FROM hmm_state_vars")
        table = curs.fetchall()

        for ele in table:
            stateVars[ele[0]][ele[1]] = ele[2]

        curs.execute("SELECT * FROM hmm_state_centroids")
        table = curs.fetchall()

        for ele in table:
            stateCentroids[ele[0]][0][0] = ele[1]
            stateCentroids[ele[0]][0][1] = ele[2]

        self.HMM = self.HMM._replace(propagator = propagator, leftEVecs = leftEigenvectors, rightEVecs = rightEigenvectors, eVals = eigenvalues,
        timescales = timescales, stateMeans = stateMeans, stateVars = stateVars, stateCentroids = stateCentroids)
        

        if self.transformedTrajs:
            for i in range(len(self.transformedTrajs)):
                tmp = self.transformedTrajs[i]
                tmp = tmp[:,:self.HMM.nDimensions]
                self.transformedTrajs[i] = tmp

        if "hmm_state_distances" in self.dbTables(conn):
            curs.execute("SELECT * FROM hmm_state_distances")
            table = curs.fetchall()

            stateDistances = np.zeros((self.HMM.nDimensions,self.HMM.nClusters,self.HMM.nClusters))

            for ele in table:
                stateDistances[ele[0]][ele[1]][ele[2]] = ele[3]

            self.HMM = self.HMM._replace(stateDistances = stateDistances)

    def GHMMAgregateClusters(self) -> None:
        """
        Calculate the Bhattacharyya distance matrices for each tICA dimension
        """

        tmpM = self.HMM.stateMeans
        tmpV = self.HMM.stateVars

        distances = np.zeros((self.HMM.nDimensions, self.HMM.nClusters, self.HMM.nClusters))

        for i in range(self.HMM.nClusters):
            for j in range(i+1, self.HMM.nClusters):
                tmp = self.diagonalBhattacharrya(tmpM[i], tmpV[i], tmpM[j], tmpV[j])

                for k in range(self.HMM.nDimensions):
                    distances[k][i][j] = tmp[k]
                    distances[k][j][i] = tmp[k]

        self.HMM = self.HMM._replace(stateDistances = distances)

    def GHMMLogAgregates(self, overwrite=False) -> None:
        conn = sqlite3.connect(self.dbPath)

        if overwrite:
            tables = self.dbTables(conn)

            if "hmm_state_distances" in tables:
                conn.execute("DROP TABLE hmm_state_distances")

        conn.execute("CREATE TABLE hmm_state_distances (dim UNSIGNED INTEGER, i UNSIGNED INTEGER, j UNSIGNED INTEGER, r REAL)")

        for i in range(len(self.HMM.stateDistances)):
            for j in range(len(self.HMM.stateDistances[0])):
                for k in range(len(self.HMM.stateDistances[0])):
                    conn.execute("INSERT INTO hmm_state_distances VALUES (?,?,?,?)", (i,j,k,self.HMM.stateDistances[i][j][k]))

        conn.commit()
        conn.close()

    def GHMMGetPopulations(self):
        poplIndex = 0
        maxSum = -math.inf
        for i in range(len(self.HMM.eVals)):
            total = sum(self.HMM.leftEVecs[i])
            if total > maxSum:
                maxSum = total
                poplIndex = i

        return self.HMM.leftEVecs[poplIndex] / maxSum

    def buildStructuralClusters(self):
        """
        Generates the connected components of the state graph by separating using the cumulative Bhattacharyya distance.
        Return format : (nDimensions, nConnectedComponents (variable), nStates (variable))
        """

        if self.HMM.stateDistances is None:
           self.GHMMAgregateClusters()

        stateDistanceClusters = []

        for i in range(len(self.HMM.stateDistances)):
            cumulativeGraph = Graph([],[])

            cumulativeDistances = sum(self.HMM.stateDistances[:i+1])

            for j in range(len(cumulativeDistances)):
                cumulativeGraph.addNewVertex(j)

            currentEdgeLabelNumber = 0

            for j in range(len(cumulativeDistances) - 1):
                for k in range(j+1,len(cumulativeDistances)):
                    if cumulativeDistances[j][k] < bhattacharyya25:
                        cumulativeGraph.addNewEdge(currentEdgeLabelNumber,j,k,False)
                        currentEdgeLabelNumber += 1

            # Reasoning : state clusters do not have connections with each other and are therefore distinct connected components
            # Instead of thinking vertically (i.e. which supercluster is parent to the cluster), we think horizontally (i.e. which states are siblings at this level)
            cumulativeSubGraphs = cumulativeGraph.subGraphs()

            # Append state numbers grouped in subgraps to stateDistanceClusters, effectively creating a tree
            tmp = []
            for subGraph in cumulativeSubGraphs:
                tmp1 = []
                for vertex in subGraph.vertices:
                    tmp1.append(vertex.label)
                tmp.append(tmp1)

            stateDistanceClusters.append(tmp)

        self.HMM = self.HMM._replace(stateDistanceClusters=stateDistanceClusters)


    def buildStructuralClusterTree(self) -> str:
        """
        Builds a tree representation using the newick format. Trees can be seen at :
        http://etetoolkit.org/treeview/ or similar. States are letter coded, with 0 associated to A, 26 with AA, and so on.
        """
        if self.HMM.stateDistanceClusters is None:
            self.buildStructuralClusters()
        
        structuralTree = Graph([],[])

        structuralTree.addNewVertex(0) #root
        vertexToContent = dict()

        currentVertexLabel = 1
        currentEdgeLabel = 0
        for i in range(len(self.HMM.stateDistanceClusters)):
            print("Level {:n}".format(i))
            for j in range(len(self.HMM.stateDistanceClusters[i])):
                currentVertex = Vertex(currentVertexLabel)
                if(i+1 == len(self.HMM.stateDistanceClusters)):
                    currentVertex.content = self.HMM.stateDistanceClusters[i][j]
                vertexToContent[currentVertexLabel] = self.HMM.stateDistanceClusters[i][j]
                structuralTree.addVertex(currentVertex)

                if i == 0:
                    structuralTree.addNewEdge(currentEdgeLabel,currentVertexLabel,0,False)
                else:
                    for k in range(currentVertexLabel-1,-1,-1):
                        if vertexToContent[currentVertexLabel][0] in vertexToContent[k]:
                            print(str(currentVertexLabel) + ": " + str(vertexToContent[currentVertexLabel]))
                            print(str(k) + ":: " + str(vertexToContent[k]))
                            structuralTree.addNewEdge(currentEdgeLabel,currentVertexLabel,k,False)
                            break
                
                currentEdgeLabel += 1
                currentVertexLabel += 1

        return self.treeToString(structuralTree) + ";"

    # sister method to buildStructuralClusterTree
    def treeToString(self,tree:Graph) -> str:
        """
        Recursively computes the Newick string format of a tree.
        Behaviour NOT guaranteed if the passed graph is not a tree. 
        """
        ret = "("

        treeCopy = deepcopy(tree)

        labels = []

        for vertex in treeCopy.vertices:
            labels.append(vertex.label)

        treeCopy.removeVertexByLabel(treeCopy.getVertexByLabel(sorted(labels)[0]).label)

        subTrees = treeCopy.subGraphs()

        for i in range(len(subTrees)):
            subTree = subTrees[i]
            if len(subTree.vertices) > 0:
                if len(subTree.vertices) == 1:
                    ret += "(" + self.intToString(subTree.vertices[0].content[0])

                    for i in range(1,len(subTree.vertices[0].content)):
                        ret += "," + self.intToString(subTree.vertices[0].content[i])
                    ret += ")"
                else:
                    ret += self.treeToString(subTree)

                if i != len(subTrees) - 1:
                    ret += ","

        ret += ")"

        return ret

    # Sister method to treeToString
    # Necessary to comply with the Newick format
    def intToString(self, num:int):
        if num == 0:
            return "A"
        else:
            ret = ""

            while(num > 0):
                if num < 26:
                    return intToMaj[num] + ret
                else:
                    ret = intToMaj[num % 26] + ret
                    num = (num // 26) - 1
                    if num == 0:
                        return "A" + ret


    def diagonalBhattacharrya(self, mean1, var1, mean2, var2) -> list:
        var12 = 0.0
        distances = []
        
        for i in range(len(var1)):
            var12 = (var1[i] + var2[i])/2.0

            distances.append(((mean1[i] - mean2[i])**2)/(8.0*var12) - math.log(var12/math.sqrt(var1[i]*var2[i]))/2.0)
        
        return distances

    def discreteDerivative(self, series, i: int, n: int) -> float:
        """
        Calculate the derivative of the data series around integer i using the Savitsky-Golay smoothing approximation
        Savitsky A, Golay MJE. Analytical Chemistry. 1964. Vol 36 (8). p1627-1639
        Source of coefficients : Gorry A. Analytical Chemistry. Vol 62 (6). p570-573
        """
        
        if not (n in standardizationFactors):
            raise Exception("Exception occured while trying to calculate a discrete derivative using an incoherent number of points : {:n}.".format(n))
        else:
            ret = 0.0

            factor = standardizationFactors[n]

            span = n >> 1

            for j in range(-span,span+1,1):
                ret += float(j)*series[i+j]/factor

            return ret