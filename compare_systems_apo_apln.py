#!usr/bin/python

from TimeSeries import TimeSeries
from MDSystem import MDSystem
from copy import copy

aplnSys = MDSystem()
# Initialize system parameters from SQL
aplnSys.initializeFromSQL("WT_apln_final.sqlite")
print(aplnSys.textFeatures)
aplnSys.controlCommonInput()
aplnSys.loadTopolFromFile("apj_apj_apln_topol.gro")
aplnSys.tICALoadResultsFromSQL()
aplnSys.loadFeatureAtomsFromSQL()
aplnSys.sequenceToFeatures()

apoSys = MDSystem()
# Initialize system parameters from SQL
apoSys.initializeFromSQL("WT_apo_final.sqlite")
print(apoSys.textFeatures)
apoSys.controlCommonInput()
apoSys.loadTopolFromFile("APJ_topol.gro")
apoSys.tICALoadResultsFromSQL()
apoSys.loadFeatureAtomsFromSQL()
apoSys.sequenceToFeatures()

"""
for key1 in sorted(apoSys.seqToFeat.keys()):
	for key2 in sorted(apoSys.seqToFeat[key1].keys()):
		print(str(key1) + ":" + str(key2) + ":" + str(apoSys.seqToFeat[key1][key2]))
		if key1 in aplnSys.seqToFeat and key2 in aplnSys.seqToFeat[key1]:
			print(str(key1) + ":" + str(key2) + ":" + str(aplnSys.seqToFeat[key1][key2]))
		else:
			print("MISSING : " + str(key1) + ":" + str(key2))
"""
"""
for i in range(len(apoSys.featToSeq)):
	print(i)
	print(apoSys.featToSeq[i][0])
	if i < len(aplnSys.featToSeq):
		print(aplnSys.featToSeq[i][0])
"""

# Remove 14-phi, 311-phi, 312-phi, 311-psi, 14-chi1 and 311-chi2
compare = TimeSeries.tICACompare(aplnSys.tICA.eVecs,[],apoSys.tICA.eVecs,[12,309,310,620,633,1077])
for i in range(len(compare)):
	print(max(compare[i]))