#!usr/bin/python

import warnings
from MDSystem import MDSystem
from TimeSeries import bhattacharyya25
from PlotUtils import plotHeatMap, plotHMMAndTrajs, plotSimple, plotMultiple
from copy import copy
import re
import os

warnings.filterwarnings("ignore", category=DeprecationWarning)

"""
Note to all users!
Pease, first take note that examples of this bug have been found only in instances of topologies that
have to be trimmed with the "include" and "exclude" statements. Now for the bug : the MDTraj suite somehow
prevents correct topology loading from SQL. A fix has been put in place : you may call
sys.loadTopolFromFile(pathToTopol) to circumvent this problem. This can be called after loading from SQL
without issues and with no impact on performance.
"""

sys = MDSystem()

# Initialize system parameters from command line
#sys.initializeFromCommandLine()
#sys.setDBPath("d75n_apo_final_fullsystem.sqlite")
#sys.controlCommonInput()
#sys.logInit(True)

sysProt = MDSystem()

sysProt.setDBPath("d75n_apo_final.sqlite")

# Initialize system parameters from SQL
sysProt.initializeFromSQL("d75n_apo_final.sqlite")

for i in range(len(sysProt.input)):
    sysProt.input[i] = re.sub(r"results_prod(\d+)", r"results_prod\1/prot",sysProt.input[i],1)

sysProt.controlCommonInput()
sysProt.loadTopolFromFile("d75n_apo_topol.gro")

# Load trajectory features
#sys.loadTrajs()

# Log feature atoms for an eventual tICA scan
#sys.logFeatureAtoms(True)

# Load feature atoms for tICA scan
#sys.loadFeatureAtomsFromSQL()

# Compute and log tICA
#sys.tICACompute()
#sys.tICALog(True)

# Load tICAfrom SQL
#sys.tICALoadResultsFromSQL()

# Perform feature-sequence associations and perform tICA scan
#sys.featureToSequence()
#sys.sequenceToFeatures()
#sys.tICASequenceScan()
#sys.tICAFeatureScan()
#result = sys.tICAFeaturePerturbationAnalysis(0,sys.tICA.nWorthwhileComps)

#top10pRatio = sorted(copy(sys.sequenceScans[0][2][0]))
#top10pRatio = top10pRatio[len(top10pRatio)//10]
#top10pRatio = list((top10pRatio,)*len(sys.sequenceScans[0][0]))
#top10pAngle = sorted(copy(sys.sequenceScans[0][1][0]))
#top10pAngle = top10pAngle[len(top10pRatio)//10]
#top10pAngle = list((top10pAngle,)*len(sys.sequenceScans[0][0]))
#plotMultiple(sys.sequenceScans[0][0], sys.sequenceScans[0][3], [sys.sequenceScans[0][2][0],sys.sequenceScans[0][1][0], top10pRatio, top10pAngle],
#["Eigenvalue ratio", "Eigenvector angle cosine", "Top 10% threshold for ratio", "Top 10% threshold for cosine"], "Residue", "Ratio", "Variation of eigenvalue and eigenvector orientation for eigenpair 1 as a function of residue for chain 0")
#top10pRatio = sorted(copy(sys.sequenceScans[0][2][1]))
#top10pRatio = top10pRatio[len(top10pRatio)//10]
#top10pRatio = list((top10pRatio,)*len(sys.sequenceScans[0][0]))
#top10pAngle = sorted(copy(sys.sequenceScans[0][1][1]))
#top10pAngle = top10pAngle[len(top10pRatio)//10]
#top10pAngle = list((top10pAngle,)*len(sys.sequenceScans[0][0]))
#plotMultiple(sys.sequenceScans[0][0], sys.sequenceScans[0][3], [sys.sequenceScans[0][2][1],sys.sequenceScans[0][1][1], top10pRatio, top10pAngle],
#["Eigenvalue ratio", "Eigenvector angle cosine", "Top 10% threshold for ratio", "Top 10% threshold for cosine"], "Residue", "Ratio", "Variation of eigenvalue and eigenvector orientation for eigenpair 2 as a function of residue for chain 0")

# Optimize and log GHMM
#sys.GMHMMOptimize() # Ignore every timescale higher than 10 ms (in frames)
#sys.GMHMMLog(True)

# Load GHMM from SQL
sysProt.GHMMLoadFromSQL()

# Calculate interstate distances for each dimension
#sys.GHMMAgregateClusters()
#sys.GHMMLogAgregates(True)

#sys.buildStructuralClusters()
#print(sys.HMM.stateDistanceClusters)

#plotHeatMap(sys.HMM.stateDistances[0],0,bhattacharyya25, "State distances along independent component 1")
#plotHeatMap(sys.HMM["model"]["stateDistances"][1],0,bhattacharyya25)
#plotHeatMap(sys.HMM["model"]["stateDistances"][2],0,bhattacharyya25)

#plotHMMAndTrajs(sys, "System frames and GHMM projection on independent component")

for i in range(len(sysProt.HMM.stateCentroids)):
    #os.system
    os.system("gmx trjconv -f {:s} -s {:s} -o {:s}_state_{:d}.pdb -pbc mol -ur compact -b {:f} -e {:f}".format(re.sub(r"_prot","",re.sub(r"/prot","/full",sysProt.solveLocation(sysProt.input[sysProt.HMM.stateCentroids[i][0][0]]))),
            sysProt.solveLocation(sysProt.tpr), sysProt.output, i,
            (float(sysProt.HMM.stateCentroids[i][0][1]) - 0.5)*sysProt.timeStep,
            (float(sysProt.HMM.stateCentroids[i][0][1]) + 0.5)*sysProt.timeStep))